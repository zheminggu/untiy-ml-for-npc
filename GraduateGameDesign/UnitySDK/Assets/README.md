# Unity SDK

## Description

This project is my undergraduate game design project, 
in order to test ml-agent and write a deep learning based AI for a specific RTS game.
This project is about to use hybrid ECS architecture to enhance the performance of this game

## Status
current project is designed by formal architecture instead of the ECS 

### Camera 
RTS camera fixed buges of the layermask detection, it have the basic function for RTS games.

For the next step of optimize this camera is to add a height detection
when the camera's height is less than a number, it will use cinemichine, change the visual angle, to show detailed image of the NPCs 

### UI
With The game manager it can start a game and change game settings for now. 
current UI haven't linked to the game yet.
 
The future work is to utilize During-game UI function.

### Game design
Need to build Scene for RTS game

### Game AI
Need to make basic AI


## Plan 
In this week, build the basic scene for RTS game
next week Finish the RTS game machanic, to be the foundamental environment of the deep learning AI


