﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Adamgu{
[CreateAssetMenu(menuName="Adamgu/Mission/Mission")]
public class Mission : ScriptableObject
{
    public Mission(string _content){
        Content=_content;
    }
     public string Content;
    
     public UnityEvent OnMissionStart;
     public UnityEvent OnMissionComplete;

}

}
