using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Adamgu{
    [CreateAssetMenu(menuName = "Adamgu/Mission/MissionDataBase")]
    public class MissionDataBase : ScriptableObject
    {
       public Mission[] MissionData;
       private int currentMissionNumber;

       public void InitMissionDataBase(int _missionOrder){
            currentMissionNumber=_missionOrder;
       }
       public Mission GetCurrentMission(){
            if(currentMissionNumber<MissionData.Length){
                return MissionData[currentMissionNumber];
            }else{
                return new Mission("Warning, No mission set");
            }
       }

        public string GetCurrentMissionContent(){

            if(currentMissionNumber<MissionData.Length){
                 return MissionData[currentMissionNumber].Content;
            }else{
                return "Warning, No mission set";
            }
        }

        public void CompleteCurrentMission(){
            
            MissionData[currentMissionNumber].OnMissionComplete.Invoke();
            SetCurrentMission();
        }
        public void SetCurrentMission(){
            if(currentMissionNumber<MissionData.Length-1){
                currentMissionNumber++;
            }
        }

        public void SetCurrentMission(string _missionName){

        }
    }

}
