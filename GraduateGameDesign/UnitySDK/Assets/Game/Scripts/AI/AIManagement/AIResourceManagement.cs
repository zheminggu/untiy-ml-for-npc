﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Adamgu
{
    public class AIResourceManagement : MonoBehaviour
    {
        // other parameters are same as the player use game utils to get other parameters
        
        private int AI_gold;
        private int AI_hourse;
        private int AI_arrow;

        private int AI_SaberNumber = 0;
        private int AI_ArcherNumber = 0;
        private int AI_RiderNumber = 0;

        private bool AI_inTheGoldMine;
        private bool AI_inTheHourseMine;
        private bool AI_inTheArrowMine;

        private GameEnum.ResourceType addType;
        private int addNumber;

        public void StartAIResourceManagement()
        {
            AI_gold = 0;
            AI_hourse = 0;
            AI_arrow = 0;
            AI_SaberNumber = 0;
            AI_RiderNumber = 0;
            AI_ArcherNumber = 0;
            StartCoroutine(AddRescource());
        }
      

        //unfinished
        private void SetAdd(GameEnum.ResourceType _addResourceType, int _addResourceNumber)
        {
            addType = _addResourceType;
            addNumber = _addResourceNumber;
        }

        private void SetAddResourceNumber(GameEnum.ResourceType _addResourceType)
        {
            switch (_addResourceType)
            {
                case GameEnum.ResourceType.Hourse:
                    AI_hourse += addNumber;
                    break;
                case GameEnum.ResourceType.Arrow:
                    AI_arrow += addNumber;
                    break;
                case GameEnum.ResourceType.Gold:
                    AI_gold += addNumber;
                    break;
                default:
                    break;
            }
        }

        private IEnumerator AddRescource()
        {

            yield return new WaitForSeconds(GameUtils.GameManagement.GetResourceManagement().ResourceAddTime);
            //add gold
            if (AI_inTheGoldMine)
            {
                SetAdd(GameEnum.ResourceType.Gold, GameUtils.GameManagement.GetResourceManagement().AddNumberInMine);
                SetAddResourceNumber(GameEnum.ResourceType.Gold);
            }
            else
            {
                SetAdd(GameEnum.ResourceType.Gold, GameUtils.GameManagement.GetResourceManagement().AddNumberOutMine);
                SetAddResourceNumber(GameEnum.ResourceType.Gold);

            }

            //add wolf
            if (AI_inTheHourseMine)
            {
                SetAdd(GameEnum.ResourceType.Hourse, GameUtils.GameManagement.GetResourceManagement().AddNumberInMine);
                SetAddResourceNumber(GameEnum.ResourceType.Hourse);

            }
            else
            {
                SetAdd(GameEnum.ResourceType.Hourse, GameUtils.GameManagement.GetResourceManagement().AddNumberOutMine);
                SetAddResourceNumber(GameEnum.ResourceType.Hourse);

            }

            //add arrow
            if (AI_inTheArrowMine)
            {
                SetAdd(GameEnum.ResourceType.Arrow, GameUtils.GameManagement.GetResourceManagement().AddNumberInMine);
                SetAddResourceNumber(GameEnum.ResourceType.Arrow);

            }
            else
            {
                SetAdd(GameEnum.ResourceType.Arrow, GameUtils.GameManagement.GetResourceManagement().AddNumberOutMine);
                SetAddResourceNumber(GameEnum.ResourceType.Arrow);
            }
            StartCoroutine(AddRescource());
        }

        public int GetResourceNumber(GameEnum.ResourceType _resoourceType)
        {

            switch (_resoourceType)
            {
                case GameEnum.ResourceType.Hourse:
                    return AI_hourse;
                case GameEnum.ResourceType.Arrow:
                    return AI_arrow;
                case GameEnum.ResourceType.Gold:
                    return AI_gold;
                default:
                    Debug.LogWarning("the resource type error");
                    break;
            }
            return 0;

        }

        public int GetHourseNumber()
        {
            return AI_hourse;
        }
        
        public int GetGoldNumber()
        {
            return AI_gold;
        }

        public int GetArrowNumber()
        {
            return AI_arrow;
        }


        #region set value
        public void SetAIInTheGoldMine(bool _value)
        {
            AI_inTheGoldMine = _value;
        }

        public void SetAIInTheHourseMine(bool _value)
        {
            AI_inTheHourseMine = _value;
        }

        public void SetAIInTheArrowMine(bool _value)
        {
            AI_inTheArrowMine = _value;
        }

        public bool SetAIGoldResourceMinus()
        {
            if (GameUtils.AI.Resource.AIHaveEnoughGold())
            {
                AI_gold -= GameUtils.GameManagement.GetResourceManagement().SaberMinusNumber;
                return true;
            }
            else
            {
                return false;
            }
          
        }

        public bool SetAIArrowResourceMinus()
        {
            if (GameUtils.AI.Resource.AIHaveEnoughArrow())
            {
                AI_arrow -= GameUtils.GameManagement.GetResourceManagement().ArcherMinusNumber;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetAIHourseResourceMinus()
        {
            if (GameUtils.AI.Resource.AIHaveEnoughHourse())
            {
                AI_hourse -= GameUtils.GameManagement.GetResourceManagement().RiderMinusNumber;
                return true;

            }
            else
            {
                return false;
            }
        }

        public void SetCharacterAdd(GameEnum.CharacterType _characterType)
        {
            switch (_characterType)
            {
                case GameEnum.CharacterType.Saber:
                    AI_SaberNumber++;
                    break;
                case GameEnum.CharacterType.Archor:
                    AI_ArcherNumber++;
                    break;
                case GameEnum.CharacterType.Rider:
                    AI_RiderNumber++;
                    break;
                default:
                    break;
            }
        }

        public void SetCharacterMinus(GameEnum.CharacterType _characterType)
        {
            switch (_characterType)
            {
                case GameEnum.CharacterType.Saber:
                    AI_SaberNumber--;
                    break;
                case GameEnum.CharacterType.Archor:
                    AI_ArcherNumber--;
                    break;
                case GameEnum.CharacterType.Rider:
                    AI_RiderNumber--;
                    break;
                default:
                    break;
            }
        }
        #endregion


    }

}
