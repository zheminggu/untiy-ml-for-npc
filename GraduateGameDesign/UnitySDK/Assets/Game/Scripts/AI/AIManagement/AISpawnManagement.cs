﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Adamgu
{
    public class AISpawnManagement : MonoBehaviour
    {
        private int targetPositionOrder = 0;


        public void StartAISpawnManagement()
        {
            StartCoroutine(AIBasicAutoCreateCharacter());
        }

        public void AICreateArcher()
        {
            if (GameManager.instance.GetGameSide() == GameEnum.CharacterSide.Blue)
            {
                // AI is on the red Side
                if (GameUtils.AI.Resource.AIHaveEnoughArrow())
                {
                    var newArchor = Instantiate(GameUtils.GameManagement.GetSpawnManagement().GetRedArcherPrefab(), GameUtils.GameManagement.GetSpawnManagement().GetRedSpawnPosition(), Quaternion.identity);
                    SelfMoveToTarget(newArchor, GameEnum.CharacterSide.Red);
                }
            }
            else
            {
                // AI is on the blue side
                if (GameUtils.AI.Resource.AIHaveEnoughArrow())
                {
                    var newArchor = Instantiate(GameUtils.GameManagement.GetSpawnManagement().GetRedArcherPrefab(), GameUtils.GameManagement.GetSpawnManagement().GetBlueSpawnPosition(), Quaternion.identity);
                    SelfMoveToTarget(newArchor, GameEnum.CharacterSide.Blue);
                }
            }
        }

    

        public void AICreateSaber()
        {
            if (GameManager.instance.GetGameSide() == GameEnum.CharacterSide.Blue)
            {
                // AI is on the red Side
                if (GameUtils.AI.Resource.AIHaveEnoughGold())
                {
                    var newSaber = Instantiate(GameUtils.GameManagement.GetSpawnManagement().GetRedSaberPrefab(), GameUtils.GameManagement.GetSpawnManagement().GetRedSpawnPosition(), Quaternion.identity);
                    SelfMoveToTarget(newSaber, GameEnum.CharacterSide.Red);
                }
            }
            else
            {
                // AI is on the blue side
                if (GameUtils.AI.Resource.AIHaveEnoughGold())
                {
                    var newSaber = Instantiate(GameUtils.GameManagement.GetSpawnManagement().GetBlueSaberPrefab(), GameUtils.GameManagement.GetSpawnManagement().GetBlueSpawnPosition(), Quaternion.identity);
                    SelfMoveToTarget(newSaber, GameEnum.CharacterSide.Blue);
                }
            }
        }

        public void AICreateRider()
        {
            if (GameManager.instance.GetGameSide() == GameEnum.CharacterSide.Blue)
            {
                // AI is on the red Side
                if (GameUtils.AI.Resource.AIHaveEnoughHourse())
                {
                    var newRider = Instantiate(GameUtils.GameManagement.GetSpawnManagement().GetRedRiderPrefab(), GameUtils.GameManagement.GetSpawnManagement().GetRedSpawnPosition(), Quaternion.identity);
                    SelfMoveToTarget(newRider, GameEnum.CharacterSide.Red);
                }
            }
            else
            {
                // AI is on the blue side
                if (GameUtils.AI.Resource.AIHaveEnoughHourse())
                {
                    var newRider = Instantiate(GameUtils.GameManagement.GetSpawnManagement().GetBlueRiderPrefab(), GameUtils.GameManagement.GetSpawnManagement().GetBlueSpawnPosition(), Quaternion.identity);
                    SelfMoveToTarget(newRider, GameEnum.CharacterSide.Blue);
                }
            }
        }

        private void SelfMoveToTarget(GameObject _gameObject, GameEnum.CharacterSide _characterSide)
        {
            if (!_gameObject)
            {
                Debug.LogError("Null gameobject can't move to target");
                return;
            }
            //start self control
            _gameObject.GetComponent<CharacterStatusComponent>().SelfControl = true;
            _gameObject.GetComponent<CharacterMoveComponent>().NeedMove = true;
            _gameObject.GetComponent<CharacterMoveComponent>().TargetPosition = GameUtils.GameManagement.GetSpawnManagement().GetFirstTargetPosition(_characterSide, GetTargetOrder());

        }

        private int GetTargetOrder()
        {
            targetPositionOrder++;
            targetPositionOrder %= 3;
            return targetPositionOrder;
        }

        private void AIAutoCreateArcher()
        {
            //for AI
            if (GameUtils.AI.Resource.AIHaveEnoughArrow())
            {
                AICreateArcher();
            }
        }

        private void AIAutoCreateSaber()
        {
            //for AI
            if (GameUtils.AI.Resource.AIHaveEnoughGold())
            {
                AICreateSaber();
            }
        }

        private void AIAutoCreateRider()
        {
            //for AI
            if (GameUtils.AI.Resource.AIHaveEnoughHourse())
            {
                AICreateRider();
            }
        }

        private IEnumerator AIBasicAutoCreateCharacter()
        {
            yield return new WaitForSeconds(GameUtils.GameManagement.GetSpawnManagement().AutoSpawnTime);
            AIAutoCreateArcher();
            AIAutoCreateSaber();
            AIAutoCreateRider();
            StartCoroutine(AIBasicAutoCreateCharacter());
        }
    }
}

