﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Adamgu
{
    public class HouseComponent:MonoBehaviour
    {
        public float Distance;
        [InfoBox(   "0 HourseMine,\n" +
                    "1  ArrowMine,\n" +
                    "2  GoldMine,\n" +
                    "3  BlueTower1,\n" +
                    "4  BlueTower2,\n" +
                    "5  BlueTower3,\n" +
                    "6  BlueBackTower1,\n" +
                    "7  BlueBackTower2,\n" +
                    "8  BlueBackTower3,\n" +
                    "9  RedTower1,\n" +
                    "10 RedTower2,\n" +
                    "11 RedTower3,\n" +
                    "12 RedBackTower1,\n" +
                    "13 RedBackTower2,\n" +
                    "14 RedBackTower3,\n")]
        [PropertyRange(0,14)]
        public int HouseType;

        public int Health;
    }
}