﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Adamgu
{
    public class HouseImpactSystem : ComponentSystem
    {
        private struct ImpactData
        {
            public readonly int Length;
            public ComponentDataArray<CharacterImpactComponent> CharacterImpactComponent;
            public ComponentArray<HouseComponent> HouseComponent;
            public EntityArray Entity;
        }

        [Inject] ImpactData _impactData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < _impactData.Length; i++)
            {
                if (!_impactData.HouseComponent[i])
                {
                    continue;
                }
                _impactData.HouseComponent[i].Health -= _impactData.CharacterImpactComponent[i].Value;
            }
        }
    }
}

