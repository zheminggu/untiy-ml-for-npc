﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;


namespace Adamgu
{
    public class HouseDeathSystem : ComponentSystem
    {
        private struct HouseData
        {
            public readonly int Length;
            public ComponentArray<HouseComponent> HouseComponent;
        }

        [Inject] HouseData _houseData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < _houseData.Length; i++)
            {
              
                if (!_houseData.HouseComponent[i])
                {
                    continue;
                }
                if (_houseData.HouseComponent[i].Health < 0)
                {
                    Object.Destroy(_houseData.HouseComponent[i].gameObject);
                }
            }
        }
    }

}
