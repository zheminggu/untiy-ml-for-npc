﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Adamgu
{
    public class HouseSystem : ComponentSystem
    {
        private struct House
        {
            public HouseComponent HouseComponent;
            public Transform Transform;
        }
        private struct Characters
        {
            public CharacterStatusComponent CharacterStatusComponent;
            public Transform Transform;
        }

        protected override void OnUpdate()
        {
            var havePlayerCharacterInTheDistance=false;
            var haveEnemyCharacterInTheDistance = false;
            foreach (var house in GetEntities<House>())
            {
                switch (GameUtils.GameManagement.Character.GetHouseType(house.HouseComponent.HouseType))
                {
                    case GameEnum.HouseType.HourseMine:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        GameUtils.GameManagement.SetPlayerInTheHourseMine(havePlayerCharacterInTheDistance);
                        //haven't finished
                        if (haveEnemyCharacterInTheDistance)
                        {
                            //haven't finished yet!!!!!!!!!!!!!!!
                        }
                        break;
                    case GameEnum.HouseType.ArrowMine:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        GameUtils.GameManagement.SetPlayerInTheArrowMine(haveEnemyCharacterInTheDistance);
                      
                        //enemy system haven't finished yet!!!!!!!!!!!!!!!
                        if (haveEnemyCharacterInTheDistance)
                        {
                            //haven't finished yet!!!!!!!!!!!!!!!
                        }
                        break;
                    case GameEnum.HouseType.GoldMine:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        GameUtils.GameManagement.SetPlayerInTheGoldMine(haveEnemyCharacterInTheDistance);

                        //enemy system haven't finished yet!!!!!!!!!!!!!!!
                        if (haveEnemyCharacterInTheDistance)
                        {
                            //haven't finished yet!!!!!!!!!!!!!!!
                        }
                        break;
                    case GameEnum.HouseType.BlueTower1:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        if (havePlayerCharacterInTheDistance)
                        {
                            //haven't finished yet!!!!!!!!!!!!!!!

                        }
                        else if (haveEnemyCharacterInTheDistance)
                        {
                            //haven't finished yet!!!!!!!!!!!!!!!
                        }
                        break;
                    case GameEnum.HouseType.BlueTower2:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);

                        break;
                    case GameEnum.HouseType.BlueTower3:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);

                        break;
                    case GameEnum.HouseType.BlueBackTower1:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);

                        break;
                    case GameEnum.HouseType.BlueBackTower2:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);

                        break;
                    case GameEnum.HouseType.BlueBackTower3:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);

                        break;
                    case GameEnum.HouseType.RedTower1:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        break;
                    case GameEnum.HouseType.RedTower2:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        break;
                    case GameEnum.HouseType.RedTower3:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        break;
                    case GameEnum.HouseType.RedBackTower1:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        break;
                    case GameEnum.HouseType.RedBackTower2:
                        HaveCharacterEnterTheDistance(house, out havePlayerCharacterInTheDistance, out haveEnemyCharacterInTheDistance);
                        break;
                    case GameEnum.HouseType.RedBackTower3:
                        break;
                    default:
                        break;
                }
           
            }
        }

        private void HaveCharacterEnterTheDistance(House house,out bool _playerCharacterInTheDistance,out bool _enemyCharacterInTheDistance)
        {
            _playerCharacterInTheDistance = false;
            _enemyCharacterInTheDistance = false;
            foreach (var character in GetEntities<Characters>())
            {
                //if enemy character and player character all in the distance,then break to set in the wood house
                //doesn't have to detect again
                if (_enemyCharacterInTheDistance && _playerCharacterInTheDistance)
                {
                    break;
                }
                //detect the distance 
                if ((house.Transform.position - character.Transform.position).sqrMagnitude < GameUtils.Math.Sqr(house.HouseComponent.Distance))
                {
                    //current character is on the player's side 
                    if (GameUtils.GameManagement.Character.CharacterSideEquals(character.CharacterStatusComponent.CharacterType, GameManager.instance.GetGameSide()))
                    {
                        _playerCharacterInTheDistance = true;
                    }
                    else//current character is on the enemy's side
                    {
                        _enemyCharacterInTheDistance = true;
                    }
                }
            }
        }
    }

}
