﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Adamgu{
    public class GameMissionManager : MonoBehaviour
    {
        public static GameMissionManager instance;
        public MissionDataBase missionDataBase;


        void Awake()
        {
            if (instance == null){
                instance = this;
            }
            else if (instance != this){
                Destroy(gameObject);
            }
            DontDestroyOnLoad(gameObject);
            
         }

         public void SetMessionBlock(GameObject _MissionBlock){
             Text missionContent=_MissionBlock.transform.Find("Mission text").GetComponent<Text>();
             missionContent.text= missionDataBase.GetCurrentMissionContent();
         }

        public void InitMission(){
            missionDataBase.InitMissionDataBase(0);
        }

         public void RecordMissionProcess(){

         }

         public void CompleteMission(){
            missionDataBase.CompleteCurrentMission();
         }
    
    }

}
