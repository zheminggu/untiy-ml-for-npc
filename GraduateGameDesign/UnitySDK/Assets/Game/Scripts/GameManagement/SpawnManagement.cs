﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Adamgu
{
    public class SpawnManagement : MonoBehaviour
    {
        public bool AutoSpawn=true;
        public float AutoSpawnTime=4;

        #region Prefab parameters
        [Title("Blue")]
        [TabGroup("Prefab")]
        public GameObject BlueArcherPrefab;

        [TabGroup("Prefab")]
        public GameObject BlueSaberPrefab;

        [TabGroup("Prefab")]
        public GameObject BlueRiderPrefab;

        [Title("Red")]
        [TabGroup("Prefab")]
        public GameObject RedArcherPrefab;

        [TabGroup("Prefab")]
        public GameObject RedSaberPrefab;

        [TabGroup("Prefab")]
        public GameObject RedRiderPrefab;
        #endregion

        #region Position Parameters
        [Title("Blue")]
        [TabGroup("Position")]
        public Vector3 BlueSpawnPosition;

        [TabGroup("Position")]
        public Vector3 [] BlueTargetPosition;

        [Title("Red")]
        [TabGroup("Position")]
        public Vector3 RedSpawnPosition;

        [TabGroup("Position")]
        public Vector3 [] RedTargetPosition;

        private int targetPositionOrder=0;
        #endregion


        #region Init
        public void StartSpawnManagement()
        {
            if (AutoSpawn)
            {
                StartCoroutine(AutoCreateCharacter());
            }
        }

        #endregion

        #region Get
        public GameObject GetBlueArcherPrefab()
        {
            return BlueArcherPrefab;
        }

        public GameObject GetBlueSaberPrefab()
        {
            return BlueSaberPrefab;
        }

        public GameObject GetBlueRiderPrefab()
        {
            return RedRiderPrefab;
        }

        public GameObject GetRedArcherPrefab()
        {
            return RedArcherPrefab;
        }

        public GameObject GetRedSaberPrefab()
        {
            return RedSaberPrefab;
        }

        public GameObject GetRedRiderPrefab()
        {
            return RedRiderPrefab;
        }

        public Vector3 GetBlueSpawnPosition()
        {
            return BlueSpawnPosition;
        }

        public Vector3 GetRedSpawnPosition()
        {
            return RedSpawnPosition;
        }

        public Vector3 GetFirstTargetPosition(GameEnum.CharacterSide _characterSide, int index)
        {
            switch (_characterSide)
            {
                case GameEnum.CharacterSide.Blue:
                    return BlueTargetPosition[index];
                case GameEnum.CharacterSide.Red:
                    return RedTargetPosition[index];
                default:
                    Debug.LogError("Error Character Side");
                    return BlueTargetPosition[0];
            }
        }
        #endregion

        #region Create Character
        public void PlayerCreateArcher()
        {
            if (GameManager.instance.GetGameSide()==GameEnum.CharacterSide.Blue)
            {
               
                if (GameUtils.GameManagement.SetResourceMinus(GameEnum.CharacterType.Archor))
                {
                    //spawn a new character
                    var newArchor = Instantiate(BlueArcherPrefab, BlueSpawnPosition, Quaternion.identity);
                    SelfMoveToTarget(newArchor, GameManager.instance.GetGameSide());
                    GameUtils.GameManagement.Character.AddCharacterNumber(GameEnum.CharacterType.Archor);
                }
             
            }
            else
            {
                if (GameUtils.GameManagement.SetResourceMinus(GameEnum.CharacterType.Archor))
                {
                    var newArchor = Instantiate(RedArcherPrefab, RedSpawnPosition, Quaternion.identity);
                    SelfMoveToTarget(newArchor, GameManager.instance.GetGameSide());
                    GameUtils.GameManagement.Character.AddCharacterNumber(GameEnum.CharacterType.Archor);
                }
              
            }
            
        }

        public void PlayerCreateSaber()
        {
            if (GameManager.instance.GetGameSide() == GameEnum.CharacterSide.Blue)
            {
                if (GameUtils.GameManagement.SetResourceMinus(GameEnum.CharacterType.Saber))
                {
                    var newSaber = Instantiate(BlueSaberPrefab, BlueSpawnPosition, Quaternion.identity);
                    SelfMoveToTarget(newSaber, GameManager.instance.GetGameSide());
                    GameUtils.GameManagement.Character.AddCharacterNumber(GameEnum.CharacterType.Saber);
                }
               
            }
            else
            {
                if (GameUtils.GameManagement.SetResourceMinus(GameEnum.CharacterType.Saber))
                {
                    var newSaber = Instantiate(RedSaberPrefab, RedSpawnPosition, Quaternion.identity);
                    SelfMoveToTarget(newSaber, GameManager.instance.GetGameSide());
                    GameUtils.GameManagement.Character.AddCharacterNumber(GameEnum.CharacterType.Saber);

                }

            }
        }

        public void PlayerCreateRider()
        {
            if (GameManager.instance.GetGameSide() == GameEnum.CharacterSide.Blue)
            {
                if (GameUtils.GameManagement.SetResourceMinus(GameEnum.CharacterType.Rider))
                {
                    var newRider = Instantiate(BlueRiderPrefab, BlueSpawnPosition, Quaternion.identity);
                    SelfMoveToTarget(newRider, GameManager.instance.GetGameSide());
                    GameUtils.GameManagement.Character.AddCharacterNumber(GameEnum.CharacterType.Rider);

                }


            }
            else
            {
                if (GameUtils.GameManagement.SetResourceMinus(GameEnum.CharacterType.Rider))
                {
                    var newRider = Instantiate(RedRiderPrefab, RedSpawnPosition, Quaternion.identity);
                    SelfMoveToTarget(newRider, GameManager.instance.GetGameSide());
                    GameUtils.GameManagement.Character.AddCharacterNumber(GameEnum.CharacterType.Rider);
                }
               
            }
        }

        private void SelfMoveToTarget(GameObject _gameObject,GameEnum.CharacterSide _characterSide)
        {
            if (!_gameObject)
            {
                Debug.LogError("Null gameobject can't move to target");
                return;
            }
            //start self control
            _gameObject.GetComponent<CharacterStatusComponent>().SelfControl = true;
            _gameObject.GetComponent<CharacterMoveComponent>().NeedMove = true;
            switch (_characterSide)
            {
                case GameEnum.CharacterSide.Blue:
                    _gameObject.GetComponent<CharacterMoveComponent>().TargetPosition = BlueTargetPosition[GetTargetOrder()];
                    break;
                case GameEnum.CharacterSide.Red:
                    _gameObject.GetComponent<CharacterMoveComponent>().TargetPosition = RedTargetPosition[GetTargetOrder()];
                    break;
                default:
                    Debug.LogError("character side error to move to target itself");
                    break;
            }
        }

        private int GetTargetOrder()
        {
            targetPositionOrder++;
            targetPositionOrder %= 3;
            return targetPositionOrder;
        }

        private void AutoCreateArcher()
        {
            //for player
            if (GameUtils.GameManagement.Resource.HaveEnoughArrow())
            {
                PlayerCreateArcher();
            }
        }

        private void AutoCreateSaber()
        {
            //for player
            if (GameUtils.GameManagement.Resource.HaveEnoughGold())
            {
                PlayerCreateSaber();
            }
        }

        private void AutoCreateRider()
        {
            //for player
            if (GameUtils.GameManagement.Resource.HaveEnoughHourse())
            {
                PlayerCreateRider();
            }
        }

        private IEnumerator AutoCreateCharacter()
        {
            yield return new WaitForSeconds(AutoSpawnTime);
            AutoCreateArcher();
            AutoCreateSaber();
            AutoCreateRider();
            StartCoroutine(AutoCreateCharacter());
        }

        #endregion

    }

}
