﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Adamgu{
    /// <summary>
    /// all the resource that player have been managed here
    /// </summary>
    public class ResourceManagement:MonoBehaviour
    {
        public int AddNumberInMine=100;
        public int AddNumberOutMine=200;

        public int SaberMinusNumber=100;
        public int ArcherMinusNumber=100;
        public int RiderMinusNumber = 100;

        public float ResourceAddTime = 2;
        
        private int gold;
        private int hourse;
        private int arrow;

        private int playerSaberNumber = 0;
        private int playerArcherNumber = 0;
        private int playerRiderNumber = 0;


        private int addNumber;
        private GameEnum.ResourceType addType;


        private bool inTheGoldMine;
        private bool inTheHourseMine;
        private bool inTheArrowMine;
        
        public void StartResourceManagement()
        {
            gold = 0;
            hourse = 0;
            arrow = 0;
            playerSaberNumber = 0;
            playerRiderNumber = 0;
            playerArcherNumber = 0;
            StartCoroutine(AddRescource());
        }

        private IEnumerator ShowAddResource(){
            GameEnum.ResourceType _addType = addType;
            GameUIManager.instance.OpenAddingResourceWindow(addType, addNumber);
            //Debug.Log(addType);
            //Debug.Log(addNumber);
            yield return new WaitForSeconds(1);
            GameUIManager.instance.CloseAddingResourceWindow(_addType);
        }

        //unfinished
        private void SetAdd(GameEnum.ResourceType _addResourceType, int _addResourceNumber)
        {
           

            addType = _addResourceType;
            addNumber = _addResourceNumber;
           
            StartCoroutine(ShowAddResource());

        }

        private void SetAddResourceNumber(GameEnum.ResourceType _addResourceType)
        {
            switch (_addResourceType)
            {
                case GameEnum.ResourceType.Hourse:
                    hourse += addNumber;
                    GameUIManager.instance.SetResourceNumber(_addResourceType,hourse);
                    break;
                case GameEnum.ResourceType.Arrow:
                    arrow += addNumber;
                    GameUIManager.instance.SetResourceNumber(_addResourceType,arrow);
                    break;
                case GameEnum.ResourceType.Gold:
                    gold += addNumber;
                    GameUIManager.instance.SetResourceNumber(_addResourceType, gold);
                    break;
                default:
                    break;
            }
        }  

        private IEnumerator AddRescource()
        {
           
            yield return new WaitForSeconds(ResourceAddTime);
            //add gold
            if (inTheGoldMine)
            {
                SetAdd(GameEnum.ResourceType.Gold, AddNumberInMine);
                SetAddResourceNumber(GameEnum.ResourceType.Gold);
            }
            else
            {
                SetAdd(GameEnum.ResourceType.Gold, AddNumberOutMine);
                SetAddResourceNumber(GameEnum.ResourceType.Gold);

            }

            //add wolf
            if (inTheHourseMine)
            {
                SetAdd(GameEnum.ResourceType.Hourse, AddNumberInMine);
                SetAddResourceNumber(GameEnum.ResourceType.Hourse);

            }
            else
            {
                SetAdd(GameEnum.ResourceType.Hourse, AddNumberOutMine);
                SetAddResourceNumber(GameEnum.ResourceType.Hourse);

            }

            //add arrow
            if (inTheArrowMine)
            {
                SetAdd(GameEnum.ResourceType.Arrow, AddNumberInMine);
                SetAddResourceNumber(GameEnum.ResourceType.Arrow);

            }
            else
            {
                SetAdd(GameEnum.ResourceType.Arrow, AddNumberOutMine);
                SetAddResourceNumber(GameEnum.ResourceType.Arrow);
            }
            StartCoroutine(AddRescource());
        }

        public int GetResourceNumber(GameEnum.ResourceType _resoourceType){
   
            switch (_resoourceType)
            {
                case GameEnum.ResourceType.Hourse:
                    return hourse;
                case GameEnum.ResourceType.Arrow:
                    return arrow;
                case GameEnum.ResourceType.Gold:
                    return gold;
                default:
                    Debug.LogWarning("the resource type error");
                    break;
            }
            return 0;

        }

        public int GetGoldMinusNumber()
        {
            return SaberMinusNumber;
        }

        public int GetArrowMinusNumber()
        {
            return ArcherMinusNumber;
        }

        public int GetHourseMinusNumber()
        {
            return RiderMinusNumber;
        }
        #region set value
        public void SetInTheGoldMine(bool _value)
        {
            inTheGoldMine = _value;
        }

        public void SetInTheHourseMine(bool _value)
        {
            inTheHourseMine = _value;
        }

        public void SetInTheArrowMine(bool _value)
        {
            inTheArrowMine = _value;
        }

        public bool SetSaberMinus()
        {
            if (gold - SaberMinusNumber >= 0)
            {
                gold -= SaberMinusNumber;
                GameUIManager.instance.SetResourceNumber(GameEnum.ResourceType.Gold, gold);
                return true;
            }
            else
            {
                GameUIManager.instance.ShowResourceWarning(GameEnum.ResourceType.Gold);
                return false;
            }
        }

        public bool SetArchorMinus()
        {
            if (arrow - ArcherMinusNumber >= 0)
            {
                arrow -= ArcherMinusNumber;
                GameUIManager.instance.SetResourceNumber(GameEnum.ResourceType.Arrow, arrow);
                //Debug.Log("Here");
                return true;
            }
            else
            {
                GameUIManager.instance.ShowResourceWarning(GameEnum.ResourceType.Arrow);
                //Debug.Log("can't minus");
                return false;
            }
        }

        public bool SetRiderMinus()
        {
            if (hourse - RiderMinusNumber >= 0)
            {
                hourse -= RiderMinusNumber;
                GameUIManager.instance.SetResourceNumber(GameEnum.ResourceType.Hourse, hourse);

                return true;

            }
            else
            {
                GameUIManager.instance.ShowResourceWarning(GameEnum.ResourceType.Hourse);
                return false;
            }
        }

        public void SetCharacterAdd(GameEnum.CharacterType _characterType)
        {
            switch (_characterType)
            {
                case GameEnum.CharacterType.Saber:
                    playerSaberNumber++;
                    GameUIManager.instance.SetPlayerCharacterNumber(_characterType,playerSaberNumber);
                    break;
                case GameEnum.CharacterType.Archor:
                    playerArcherNumber++;
                    GameUIManager.instance.SetPlayerCharacterNumber(_characterType, playerArcherNumber);
                    break;
                case GameEnum.CharacterType.Rider:
                    playerRiderNumber++;
                    GameUIManager.instance.SetPlayerCharacterNumber(_characterType, playerRiderNumber);
                    break;
                default:
                    break;
            }
        }

        public void SetCharacterMinus(GameEnum.CharacterType _characterType)
        {
            switch (_characterType)
            {
                case GameEnum.CharacterType.Saber:
                    playerSaberNumber--;
                    GameUIManager.instance.SetPlayerCharacterNumber(_characterType, playerSaberNumber);
                    break;
                case GameEnum.CharacterType.Archor:
                    playerArcherNumber--;
                    GameUIManager.instance.SetPlayerCharacterNumber(_characterType, playerArcherNumber);
                    break;
                case GameEnum.CharacterType.Rider:
                    playerRiderNumber--;
                    GameUIManager.instance.SetPlayerCharacterNumber(_characterType, playerRiderNumber);
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}