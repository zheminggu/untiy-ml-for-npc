﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    public class GameInputManager : MonoBehaviour
    {
       

        private Ray ray;
        private RaycastHit hit;

        private bool isSelecting = false;
        private Vector3 mouseStartPosition;

        public static GameInputManager instance;

        void Awake()
        {
            //Check if instance already exists
            if (instance == null)
            {
                //if not, set instance to this
                instance = this;
            }

            //If instance already exists and it's not this:
            else if (instance != this)
            {
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);
            }

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

        }


        // Update is called once per frame
        void Update()
        {
            if (!GameUtils.GameManagement.GameState.GameStarted())
            {
                return;
            }
            if (Input.GetMouseButtonDown(0))
            {
                isSelecting = true;
                mouseStartPosition = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                isSelecting = false;
            }
            if (Input.GetMouseButtonDown(1))
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    SetClickMove(hit.point); 
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameUIManager.instance.OpenSettingsMenu();
                GameManager.instance.SetCurrentGameState(GameEnum.GameState.Pause);
            }
            //if (GameManager.instance.GamePaused() && Input.GetMouseButtonDown(0))
            //{
            //    GameManager.instance.SetCurrentGameState(GameEnum.GameState.Run);
            //}

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                GameUtils.GameManagement.Camera.CameraSwiftMove();
            }
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                GameUtils.GameManagement.Camera.CameraNormalMove();
            }
        }

        private void SetClickMove(Vector3 _position)
        {
            if (GameUIManager.instance.ClickMove)
            {
                GameUIManager.instance.ClickMove.transform.position = new Vector3(_position.x,_position.y+1.2f,_position.z);
                GameUIManager.instance.ClickMove.SetActive(false);
                GameUIManager.instance.ClickMove.SetActive(true);
            }
            else
            {
                Debug.Log("Click move havent been set");
            }
        }
        
        public bool IsSelecting()
        {
            return isSelecting;
        }

        public Vector3 GetMouseStartPosition()
        {
            return mouseStartPosition;
        }
    }

}
