﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    public class AIManager : MonoBehaviour
    {
        public static AIManager instance;
        private AIResourceManagement AI_ResourceManagement;
        private AISpawnManagement AI_SpawnManagement;
        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

        }

        private void Start()
        {
            InitReference();
        }

        private void InitReference()
        {
            AI_ResourceManagement = GetComponentInChildren<AIResourceManagement>();
            AI_SpawnManagement = GetComponentInChildren<AISpawnManagement>();
        }


        public AIResourceManagement GetAIResourceManagement()
        {
            return AI_ResourceManagement;
        }

        public AISpawnManagement GetAISpawnManagement()
        {
            return AI_SpawnManagement;
        }
    }

}
