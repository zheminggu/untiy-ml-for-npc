﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Adamgu.Test
{
    public class Debugger : MonoBehaviour
    {

#if UNITY_EDITOR
    
        public MoveToTarget moveToTarget;

        public Vector3 targetPosition;
        private void Start()
        {
            
        }

        private void OnDrawGizmos()
        {
            if (!moveToTarget)
            {
                return;
            }
            if (moveToTarget.IsMouseButtonDown())
            {
                targetPosition = moveToTarget.GetTargetPosition();
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(moveToTarget.GetTargetPosition(), 1);

            }
        }



#endif
        public void AdamHelperDebugInformation(string _info)
        {
            Debug.Log(_info);
        }
    }


}

