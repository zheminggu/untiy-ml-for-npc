﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using Sirenix.OdinInspector;

//namespace wyyCloud
//{
//    public class MakeTex3D :SerializedMonoBehaviour
//    {
//        [ShowInInspector]
//        Texture3D texture;

//        // Use this for initialization
//        void Start()
//        {
//            texture = CreateTexture3D(16);
//            AssetDatabase.CreateAsset(texture, "Assets/tex3d_01.asset");
//        }

//        Texture3D CreateTexture3D(int size)
//        {
//            Color[] colorArray = new Color[size * size * size];
//            texture = new Texture3D(size, size, size, TextureFormat.RGBA32, true);
//            float r = 1.0f / (size - 1.0f);
//            Vector3 center = new Vector3(size / 2, size / 2, size / 2);
//            for (int x = 0; x < size; x++)
//            {
//                for (int y = 0; y < size; y++)
//                {
//                    for (int z = 0; z < size; z++)
//                    {
//                        Color c = new Color(x * r, y * r, z * r, 1.0f);
//                        colorArray[x + (y * size) + (z * size * size)] = c;

//                    }
//                }
//            }
//            texture.SetPixels(colorArray);
//            texture.Apply();
//            return texture;
//        }
//    }
//}