﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Adamgu.Test
{
    /// <summary>
    /// This class is used to test the navmesh agent of the unity 
    /// How the agent stop, resume and move to a target 
    /// And test the navmesh to move the character to different ways then get the right way of the navmesh baking
    /// </summary>
    public class MoveToTarget : MonoBehaviour
    {
        Ray ray;
        RaycastHit hit;
        // Start is called before the first frame update
        Vector3 targetPosition;
        bool mouseButtonDown=false;

        NavMeshAgent character;
        bool hitRightMouseButtonOnce=false;

        private void Start()
        {
            character = GetComponent<NavMeshAgent>();
            targetPosition = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if (!GameUtils.GameManagement.GameState.GameStarted())
            {
                return;
            }
            if (Input.GetMouseButtonDown(0))
            {
                StartCoroutine(SetMouseButtonDown());
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray,out hit))
                {
                    
                    targetPosition=hit.point;
                    //Debug.Log(targetPosition);
                    CharacterMoveToTarget();
                }
            }
           

            if (Input.GetMouseButtonDown(1))
            {
                hitRightMouseButtonOnce = !hitRightMouseButtonOnce;
                if (hitRightMouseButtonOnce)
                {
                    CharacterStop();
                }
                else
                {
                    CharacterResume();
                }
            }
        }
        IEnumerator SetMouseButtonDown()
        {
            mouseButtonDown = true;
            yield return new WaitForSeconds(2);
            mouseButtonDown = false;
        }

        private void CharacterMoveToTarget()
        {
            character.SetDestination(targetPosition);
        }

        private void CharacterStop()
        {
            character.isStopped = true;
        }

        private void CharacterResume()
        {
            character.isStopped = false;
        }

        #region testing
        public bool IsMouseButtonDown()
        {
            return mouseButtonDown;
        }

        public Vector3 GetTargetPosition()
        {
            return targetPosition;
        }
        #endregion
    }

}
