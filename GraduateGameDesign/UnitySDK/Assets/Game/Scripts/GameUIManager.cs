﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using System;

namespace Adamgu
{
    public class GameUIManager : MonoBehaviour
    {
        [Title("Common")]
        public bool multiplayer;
        [Title("UI Related Prefab Links")]
        public bool UIRelatedPrefabLinks;
        public static GameUIManager instance;

        #region Panel Variables

        [TabGroup("Panels")]
        public GameObject StartPanel;

        [TabGroup("Panels")]
        public GameObject MissionPanel;

        [TabGroup("Panels")]
        public GameObject GameOverPanel;

        [TabGroup("Panels")]
        public GameObject VictoryPanel;

        [TabGroup("Panels")]
        public GameObject GamePanel;

        [TabGroup("Panels")]
        public GameObject SettingsMenu;

        #endregion

        #region Setting Variables
        [TabGroup("Settings")]
        public GameObject GeneralPanel;

        [TabGroup("Settings")]
        public GameObject MusicPanel;

        [TabGroup("Settings")]
        public GameObject LightingPanel;

        [TabGroup("Settings")]
        public GameObject CameraPanel;

        #endregion

        #region  Game Variables

        [Title("character generate button")]
        [TabGroup("Game")]
        public GameObject CharacterButtons;

        [Title("Resource Number")]
        [TabGroup("Game")]
        public GameObject GoldNumber;

        [TabGroup("Game")]
        public GameObject WolfNumber;

        [TabGroup("Game")]
        public GameObject ArrowNumber;

        [TabGroup("Game")]
        public GameObject ResourceWarning;

        [TabGroup("Game")]
        public GameObject AddGoldNumber;

        [TabGroup("Game")]
        public GameObject AddWolfNumber;

        [TabGroup("Game")]
        public GameObject AddArrowNumber;

        [TabGroup("Game")]
        public GameObject SaberNumber;

        [TabGroup("Game")]
        public GameObject ArchorNumber;

        [TabGroup("Game")]
        public GameObject RiderNumber;

        [Title("Camera")]
        [TabGroup("Game")]
        public GameObject MiniCameraMask;

        [TabGroup("Game")]
        public GameObject ShowMiniCameraButton;

        [TabGroup("Game")]
        public GameObject HideMiniCameraButton;
        #endregion


        #region Cursor Variables
        [TabGroup("Cursor")]
        public Texture2D CursorTexture;
        [TabGroup("Cursor")]
        public GameObject ClickAttack;
        [TabGroup("Cursor")]
        public GameObject ClickDepoly;
        [TabGroup("Cursor")]
        public GameObject ClickMove;
        [TabGroup("Cursor")]
        public GameObject ClickPatrol;
        [TabGroup("Cursor")]
        public GameObject ClickRepair;
        #endregion
        #region init
        void Awake()
        {
            //Check if instance already exists
            if (instance == null){
                //if not, set instance to this
                instance = this;
            }

            //If instance already exists and it's not this:
            else if (instance != this){
                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);
            }

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
        
        }
        
        public void InitUI()
        {
	    	Cursor.SetCursor(CursorTexture, Vector2.zero, CursorMode.Auto);
           
            //if UI panel lost find all UI panels
            if(!StartPanel)
                StartPanel  = GameObject.Find("Start panel");
            
            if(!GameOverPanel)
               GameOverPanel = GameObject.Find("Game over panel");
            
            if(!VictoryPanel)
                VictoryPanel = GameObject.Find("Victory panel");
            
            if(! GamePanel)
                 GamePanel = GameObject.Find("Game panel");
            
            if(!GeneralPanel)
                GeneralPanel = GameObject.Find("General panel");
            
            if(!MusicPanel)
                MusicPanel = GameObject.Find("Music panel");
            
            if(!LightingPanel)
                LightingPanel = GameObject.Find("Lighting panel");
          
            if(!CameraPanel)
                CameraPanel = GameObject.Find("Camera panel");
            
            if(!SettingsMenu)
                SettingsMenu = GameObject.Find("Settings menu content");



            SettingsMenu.SetActive(false);
            MusicPanel.SetActive(false);
            CameraPanel.SetActive(false);
            LightingPanel.SetActive(false);

            //setting activetes
            StartPanel.SetActive(true);
            GameOverPanel.SetActive(false);
            VictoryPanel.SetActive(false);
            GamePanel.SetActive(false);

            // CharacterButtons.SetActive(true);

            //set mini map
            ShowMiniCameraButton.SetActive(true);
            HideMiniCameraButton.SetActive(false);
            MiniCameraMask.SetActive(false);

        }

        #endregion

        #region Start Panel


        public void SetMission(){
            MissionPanel.SetActive(true);
            GameMissionManager.instance.SetMessionBlock(MissionPanel);
        }
        public void StartNewGame(){
            StartPanel.SetActive(false);
            GamePanel.SetActive(true);
        }
        #endregion

        #region Game Panel

        public void OpenAddingResourceWindow(GameEnum.ResourceType _resourceType,int _number){

            //Debug.Log(_resourceType);
            switch(_resourceType){
                case GameEnum.ResourceType.Arrow:
                    AddArrowNumber.SetActive(true);
                    AddArrowNumber.GetComponent<Text>().text = "+" + _number.ToString();
                
                break;
                case GameEnum.ResourceType.Gold:
                    AddGoldNumber.SetActive(true);
                    AddGoldNumber.GetComponent<Text>().text = "+" + _number.ToString();
                break;
                case GameEnum.ResourceType.Hourse:
                    AddWolfNumber.SetActive(true);
                    AddWolfNumber.GetComponent<Text>().text = "+" + _number.ToString();
                break;
              
                default:
                Debug.Log("The resource type set error");
                break;
            }
      
            
        }

        public void CloseAddingResourceWindow(GameEnum.ResourceType _resourceType)
        {
            switch (_resourceType)
            {
                case GameEnum.ResourceType.Arrow:
                    AddArrowNumber.SetActive(false);
                    break;
                case GameEnum.ResourceType.Gold:
                    AddGoldNumber.SetActive(false);
                    break;
                case GameEnum.ResourceType.Hourse:
                    AddWolfNumber.SetActive(false);
                    break;
                default:
                    Debug.Log("The resource type set error");
                    break;
            }
        }

        public void SetPlayerCharacterNumber(GameEnum.CharacterType _characterType, int playerCharacterNumber)
        {
            //Debug.Log(playerCharacterNumber);
            switch (_characterType)
            {
                case GameEnum.CharacterType.Saber:
                    SaberNumber.GetComponentInChildren<Text>().text = "Saber: "+playerCharacterNumber.ToString();
                    break;
                case GameEnum.CharacterType.Archor:
                    ArchorNumber.GetComponentInChildren<Text>().text = "Archor: " + playerCharacterNumber.ToString();
                    break;
                case GameEnum.CharacterType.Rider:
                    RiderNumber.GetComponentInChildren<Text>().text = "Rider: " + playerCharacterNumber.ToString();
                    break;
                default:
                    break;
            }
        }

        public void SetResourceNumber(GameEnum.ResourceType _resourceType,int _number)
        {
            switch (_resourceType)
            {
                case GameEnum.ResourceType.Hourse:
                    WolfNumber.GetComponentInChildren<Text>().text = "Hourse: "+_number.ToString();
                    break;
                case GameEnum.ResourceType.Arrow:
                    ArrowNumber.GetComponentInChildren<Text>().text = "Arrow: "+_number.ToString();
                    break;
                case GameEnum.ResourceType.Gold:
                    GoldNumber.GetComponentInChildren<Text>().text = "Gold: "+_number.ToString();

                    break;
                default:
                    Debug.Log("resource Type error");
                    break;
            }
        }
   
        public void ShowResourceWarning(GameEnum.ResourceType _resourceType)
        {
            ResourceWarning.SetActive(true);
            switch (_resourceType)
            {
                case GameEnum.ResourceType.Hourse:
                    
                    ResourceWarning.GetComponent<Text>().text = "Not enough Hourse to deploy riders";
                    break;
                case GameEnum.ResourceType.Arrow:
                    ResourceWarning.GetComponent<Text>().text = "Not enough Arrow to deploy archors";
                    break;
                case GameEnum.ResourceType.Gold:
                    ResourceWarning.GetComponent<Text>().text = "Not enough Gold to deploy solders";
                    break;
                default:
                    break;
            }
            StartCoroutine(CloseResourceWarningWindow());
        }

        private IEnumerator CloseResourceWarningWindow()
        {
            yield return new WaitForSeconds(2);
            ResourceWarning.SetActive(false);
        }
     
        public void OpenMiniCamera()
        {
            ShowMiniCameraButton.SetActive(false);
            HideMiniCameraButton.SetActive(true);
            MiniCameraMask.SetActive(true);
        }

        public void CloseMiniCamera()
        {
            ShowMiniCameraButton.SetActive(true);
            HideMiniCameraButton.SetActive(false);
            MiniCameraMask.SetActive(false);
        }

        #endregion


        #region Settings Panel
        //Set general panel active when you click the gear icon
        public void GeneralPanelActive(){
            MusicPanel.SetActive(false);
            LightingPanel.SetActive(false);
            CameraPanel.SetActive(false);
            GeneralPanel.SetActive(true);
        }
        
        //Set music panel active when you click the music icon
        public void MusicPanelActive(){
            GeneralPanel.SetActive(false);
            LightingPanel.SetActive(false);
            CameraPanel.SetActive(false);
            MusicPanel.SetActive(true);
        }
        
        //Set lighting panel active when you click the light icon
        public void LightningPanelActive(){
            MusicPanel.SetActive(false);
            GeneralPanel.SetActive(false);
            CameraPanel.SetActive(false);
            LightingPanel.SetActive(true);
        }
        
        public void CameraPanelActive(){
            MusicPanel.SetActive(false);
            GeneralPanel.SetActive(false);
            LightingPanel.SetActive(false);
            CameraPanel.SetActive(true);
        }
        
        //Open whole settings menu when gear icon in top left corner is clicked
        public void OpenSettingsMenu(){
            if(!multiplayer){
            Time.timeScale = 0;
            }
            SettingsMenu.SetActive(true);
        }

        //Close whole settings menu when arrow icon in top left corner is clicked
        public void CloseSettingsMenu(){
            if(!multiplayer){
            Time.timeScale = 1;
            }
            SettingsMenu.SetActive(false);
            if (GameUtils.GameManagement.GameState.GamePaused())
            {
               GameUtils.GameManagement.GameState.SetCurrentGameState(GameEnum.GameState.Run);
            }
        }

        #endregion

        #region Unity Methods

        private void OnGUI()
        {

            if (GameInputManager.instance.IsSelecting())
            {
                // Create a rect from both mouse positions
                var rect = UIUtils.GetScreenRect(GameInputManager.instance.GetMouseStartPosition(), Input.mousePosition);
                UIUtils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
                UIUtils.DrawScreenRectBorder(rect, 2, new Color(0.8f, 0.8f, 0.95f));
            }
          
        }
        #endregion
    }

}
