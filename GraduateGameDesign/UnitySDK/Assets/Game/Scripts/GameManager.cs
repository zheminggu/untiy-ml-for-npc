﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Adamgu
{

    public class GameManager : MonoBehaviour
    {
       
        public static GameManager instance = null;

        private EntityManager EntityManager;
        private GameEnum.GameState currentGameState = GameEnum.GameState.Begin;
        private RTS_Cam.RTS_Camera RTSCamera;
        private GameEnum.CharacterSide gameSide=GameEnum.CharacterSide.Blue;
        private ResourceManagement resourceManagement;
        private SpawnManagement spawnManagement;


        private float CameraNormalMoveSpeed = 5;
        private float CameraSwiftMoveSpeed = 20;
        #region Initiate
        //Awake is always called before any Start functions
        void Awake()
        {
            //Check if instance already exists
            if (instance == null)

                //if not, set instance to this
                instance = this;

            //If instance already exists and it's not this:
            else if (instance != this)

                //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
                Destroy(gameObject);

            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
            
        }

        private void Start()
        {
            InitEntityManager();
            InitGame();
            

        }

        private void InitEntityManager()
        {
            instance.EntityManager = World.Active.GetExistingManager<EntityManager>();
        }

        private void InitGame()
        {
            InitGameState();
            InitCamera();
            GetReferences();

            GameUIManager.instance.InitUI();
            GameMissionManager.instance.InitMission();

        }

      

        //read game state data of this game
        private void InitGameState()
        {
            currentGameState = GameEnum.GameState.Begin;
        }

        private void InitCamera(){
            if(!RTSCamera){
                RTSCamera=GameObject.Find("RTS_Camera").GetComponent<RTS_Cam.RTS_Camera>();
            }
            RTSCamera.LockCamera(true);
            
        }
    
        private void GetReferences()
        {
            resourceManagement = instance.GetComponentInChildren<ResourceManagement>();
            spawnManagement = instance.GetComponentInChildren<SpawnManagement>();
        }
        #endregion

        #region public methods
        public void StartANewGame(){
            currentGameState= GameEnum.GameState.Run;
            RTSCamera.LockCamera(false);
            GameUIManager.instance.StartNewGame();
            resourceManagement.StartResourceManagement();
            spawnManagement.StartSpawnManagement();

            GameUtils.AI.GetAIResourceManagement().StartAIResourceManagement();
            GameUtils.AI.GetAISpawnManagement().StartAISpawnManagement();

        }

        public void ExitGame(){
            Application.Quit();
        }

        #endregion

        #region Game State
        public void SetCurrentGameState(GameEnum.GameState _gameState)
        {
            currentGameState = _gameState;
        }
        #endregion
       
        #region Get Methods
        public GameEnum.CharacterSide GetGameSide()
        {
            return gameSide;
        }

        public ResourceManagement GetResourceManagement()
        {
            return resourceManagement;
        }

        public EntityManager GetEntityManager()
        {
            return EntityManager;
        }

        public GameEnum.GameState GetCurrentGameState()
        {
            return currentGameState;
        }

        public SpawnManagement GetSpawnManagement()
        {
            return spawnManagement;
        }

        public RTS_Cam.RTS_Camera GetRTSCamera()
        {
            return RTSCamera;
        }

        public float GetCameraSwiftMoveSpeed()
        {
            return CameraSwiftMoveSpeed;
        }

        public float GetCameraNormalMoveSpeed()
        {
            return CameraNormalMoveSpeed;
        }
        #endregion


    }
}
