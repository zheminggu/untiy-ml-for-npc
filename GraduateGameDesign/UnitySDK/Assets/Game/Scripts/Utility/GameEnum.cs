﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu{
    public class GameEnum
    {
        public enum GameState
        {
            Pause,
            Begin,
            Run,
        }
        public enum ResourceType
        {
            Hourse,
            Arrow,
            Gold,
         }

        public enum CharacterSide
        {
            Blue,
            Red,
        }

        public enum CharacterType
        {
            Saber,
            Archor,
            Rider,
        }

        public enum HouseType
        {
            HourseMine,
            ArrowMine,
            GoldMine,
            BlueTower1,
            BlueTower2,
            BlueTower3,
            BlueBackTower1,
            BlueBackTower2,
            BlueBackTower3,
            RedTower1,
            RedTower2,
            RedTower3,
            RedBackTower1,
            RedBackTower2,
            RedBackTower3,
        }
       
    }
}
