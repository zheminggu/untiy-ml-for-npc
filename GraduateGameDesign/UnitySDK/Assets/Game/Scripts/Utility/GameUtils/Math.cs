﻿using Unity.Mathematics;
using UnityEngine;

namespace Adamgu.GameUtils
{
    public class Math
    {
        #region MathMatics
        public static float3 Vec3ToFloat3(Vector3 _vector3)
        {
            float3 _float3;
            _float3.x = _vector3.x;
            _float3.y = _vector3.y;
            _float3.z = _vector3.z;
            return _float3;
        }

        public static float SqrMagnitude(float3 _float3)
        {
            return _float3.x * _float3.x + _float3.y * _float3.y + _float3.z * _float3.z;
        }

        public static float Sqr(float _float)
        {
            return _float * _float;
        }
        #endregion
    }

}
