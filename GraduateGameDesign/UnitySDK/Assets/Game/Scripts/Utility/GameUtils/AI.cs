﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu.GameUtils {
    public class AI
    {
        /// <summary>
        /// have't finished yet
        /// </summary>
        public static class Resource
        {
            #region judgement
            //have't finished yet!!!!!!
            public static bool AIHaveEnoughGold()
            {
                if (AIManager.instance.GetAIResourceManagement().GetGoldNumber()-GameManager.instance.GetResourceManagement().SaberMinusNumber>=0)
                {
                    return true;
                }
                return false;
            }

            public static bool AIHaveEnoughArrow()
            {
                if (AIManager.instance.GetAIResourceManagement().GetArrowNumber() - GameManager.instance.GetResourceManagement().ArcherMinusNumber >= 0)
                {
                    return true;
                }
                return false;
            }

            public static bool AIHaveEnoughHourse()
            {
                if (AIManager.instance.GetAIResourceManagement().GetHourseNumber() - GameManager.instance.GetResourceManagement().RiderMinusNumber >= 0)
                {
                    return true;
                }
                return false;
            }
            #endregion

            #region AI create
            public static void AICreateSaber()
            {
                AIManager.instance.GetAISpawnManagement().AICreateSaber();
            }

            public static void AICreateArcher()
            {
                AIManager.instance.GetAISpawnManagement().AICreateArcher();
            }

            public static void AICreateRider()
            {
                AIManager.instance.GetAISpawnManagement().AICreateRider();
            }
            #endregion
        }


        public static AIResourceManagement GetAIResourceManagement()
        {
            return AIManager.instance.GetAIResourceManagement();
        }
        public static AISpawnManagement GetAISpawnManagement()
        {
            return AIManager.instance.GetAISpawnManagement();
        }
    }
}


