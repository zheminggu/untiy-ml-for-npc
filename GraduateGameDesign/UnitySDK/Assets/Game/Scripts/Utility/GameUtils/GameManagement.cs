﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu.GameUtils
{
    public class GameManagement
    {

        public static class GameState
        {
            public static bool GameStarted()
            {
                if (GameManager.instance.GetCurrentGameState() == GameEnum.GameState.Run)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public static bool GamePaused()
            {
                if (GameManager.instance.GetCurrentGameState() == GameEnum.GameState.Pause)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public static void SetCurrentGameState(GameEnum.GameState _gameState)
            {
                GameManager.instance.SetCurrentGameState(_gameState);
            }
        }

        public static class Character
        {
            #region Get enum  Methods
            

            public static GameEnum.CharacterType GetCharacterType(int _typeNumber)
            {
                switch (_typeNumber)
                {
                    case 0:
                        return GameEnum.CharacterType.Saber;
                    case 1:
                        return GameEnum.CharacterType.Archor;
                    case 2:
                        return GameEnum.CharacterType.Rider;
                    default:
                        Debug.LogError("Character Type error");
                        return GameEnum.CharacterType.Saber;
                }
            }

            public static GameEnum.CharacterSide GetCharacterSide(int _typeNumber)
            {
                switch (_typeNumber)
                {
                    case 0:
                        return GameEnum.CharacterSide.Blue;
                    case 1:
                        return GameEnum.CharacterSide.Red;
                    default:
                        Debug.Log("character side number error");
                        return GameEnum.CharacterSide.Blue;
                }
            }

            public static GameEnum.HouseType GetHouseType(int _typeNumber)
            {
                switch (_typeNumber)
                {
                    case 0:
                        return GameEnum.HouseType.HourseMine;
                    case 1:
                        return GameEnum.HouseType.ArrowMine;
                    case 2:
                        return GameEnum.HouseType.GoldMine;
                    case 3:
                        return GameEnum.HouseType.BlueTower1;
                    case 4:
                        return GameEnum.HouseType.BlueTower2;
                    case 5:
                        return GameEnum.HouseType.BlueTower3;
                    case 6:
                        return GameEnum.HouseType.BlueBackTower1;
                    case 7:
                        return GameEnum.HouseType.BlueBackTower2;
                    case 8:
                        return GameEnum.HouseType.BlueBackTower3;
                    case 9:
                        return GameEnum.HouseType.RedTower1;
                    case 10:
                        return GameEnum.HouseType.RedTower2;
                    case 11:
                        return GameEnum.HouseType.RedTower3;
                    case 12:
                        return GameEnum.HouseType.RedBackTower1;
                    case 13:
                        return GameEnum.HouseType.RedBackTower2;
                    case 14:
                        return GameEnum.HouseType.RedBackTower3;

                    default:
                        Debug.LogError("House Type Error");
                        return GameEnum.HouseType.HourseMine;
                }
            }
            #endregion

            #region Equals
            public static bool CharacterTypeEquals(int _typeNumber, GameEnum.CharacterType _characterType)
            {
                switch (_characterType)
                {
                    case GameEnum.CharacterType.Saber:
                        if (_typeNumber == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case GameEnum.CharacterType.Archor:
                        if (_typeNumber == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case GameEnum.CharacterType.Rider:
                        if (_typeNumber == 2)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        Debug.LogError("Error character Type to Compare");
                        return false;
                }
            }

            public static bool CharacterSideEquals(int _sideNumber, GameEnum.CharacterSide _characterType)
            {
                switch (_characterType)
                {
                    case GameEnum.CharacterSide.Blue:
                        if (_sideNumber == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    case GameEnum.CharacterSide.Red:
                        if (_sideNumber == 1)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }

                    default:
                        Debug.LogError("cahracter side Error");
                        return false;
                }
            }

            public static bool HouseSideEquals(int _houseNumber, GameEnum.HouseType _houseType)
            {
                switch (_houseType)
                {
                    case GameEnum.HouseType.HourseMine:
                        if (_houseNumber == 0)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.ArrowMine:
                        if (_houseNumber == 1)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.GoldMine:
                        if (_houseNumber == 2)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.BlueTower1:
                        if (_houseNumber == 3)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.BlueTower2:
                        if (_houseNumber == 4)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.BlueTower3:
                        if (_houseNumber == 5)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.BlueBackTower1:
                        if (_houseNumber == 6)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.BlueBackTower2:
                        if (_houseNumber == 7)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.BlueBackTower3:
                        if (_houseNumber == 8)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.RedTower1:
                        if (_houseNumber == 9)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.RedTower2:
                        if (_houseNumber == 10)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.RedTower3:
                        if (_houseNumber == 11)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.RedBackTower1:
                        if (_houseNumber == 12)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.RedBackTower2:
                        if (_houseNumber == 13)
                        {
                            return true;
                        }
                        break;
                    case GameEnum.HouseType.RedBackTower3:
                        if (_houseNumber == 14)
                        {
                            return true;
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }

            public static bool CharacterIsRider(int _typeNumber)
            {
                if (_typeNumber==2)
                {
                    return true;
                }
                return false;
            }
            #endregion

            #region Set
            public static void SetCharacterType(out int _needSet, GameEnum.CharacterType _wantedType)
            {
                switch (_wantedType)
                {
                    case GameEnum.CharacterType.Saber:
                        _needSet = 0;
                        break;
                    case GameEnum.CharacterType.Archor:
                        _needSet = 1;
                        break;
                    case GameEnum.CharacterType.Rider:
                        _needSet = 2;
                        break;
                    default:
                        Debug.LogError("Character Can't Set Type With error wanted Type");
                        _needSet = 0;
                        break;
                }
            }

            public static void SetCharacterSide(out int _needSet, GameEnum.CharacterSide _wantedSide)
            {
                switch (_wantedSide)
                {
                    case GameEnum.CharacterSide.Blue:
                        _needSet = 0;
                        break;
                    case GameEnum.CharacterSide.Red:
                        _needSet = 1;
                        break;
                    default:
                        Debug.LogError("Can't set character side");
                        _needSet = 0;
                        break;
                }
            }

            public static void SetHouseType(out int _needSet, GameEnum.HouseType _wantedType)
            {
                switch (_wantedType)
                {
                    case GameEnum.HouseType.HourseMine:
                        _needSet = 0;
                        break;
                    case GameEnum.HouseType.ArrowMine:
                        _needSet = 1;
                        break;
                    case GameEnum.HouseType.GoldMine:
                        _needSet = 2;
                        break;
                    case GameEnum.HouseType.BlueTower1:
                        _needSet = 3;
                        break;
                    case GameEnum.HouseType.BlueTower2:
                        _needSet = 4;
                        break;
                    case GameEnum.HouseType.BlueTower3:
                        _needSet = 5;
                        break;
                    case GameEnum.HouseType.BlueBackTower1:
                        _needSet = 6;
                        break;
                    case GameEnum.HouseType.BlueBackTower2:
                        _needSet = 7;
                        break;
                    case GameEnum.HouseType.BlueBackTower3:
                        _needSet = 8;
                        break;
                    case GameEnum.HouseType.RedTower1:
                        _needSet = 9;
                        break;
                    case GameEnum.HouseType.RedTower2:
                        _needSet = 10;
                        break;
                    case GameEnum.HouseType.RedTower3:
                        _needSet = 11;
                        break;
                    case GameEnum.HouseType.RedBackTower1:
                        _needSet = 12;
                        break;
                    case GameEnum.HouseType.RedBackTower2:
                        _needSet = 13;
                        break;
                    case GameEnum.HouseType.RedBackTower3:
                        _needSet = 14;
                        break;
                    default:
                        Debug.LogError("can't set house type");
                        _needSet = 0;
                        break;
                }
            }
            #endregion

            #region CharacterNumber
            public static void AddCharacterNumber(GameEnum.CharacterType _characterType)
            {
                GameManager.instance.GetResourceManagement().SetCharacterAdd(_characterType);

            }

            public static void MinusCharacterNumber(GameEnum.CharacterType _characterType)
            {
                GameManager.instance.GetResourceManagement().SetCharacterAdd(_characterType);
            }
            #endregion
        }

        public static class Resource
        {

            public static bool HaveEnoughGold()
            {
                if (GameManager.instance.GetResourceManagement().GetResourceNumber(GameEnum.ResourceType.Gold)- GameManager.instance.GetResourceManagement().GetGoldMinusNumber()< 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            public static bool HaveEnoughArrow()
            {
                if (GameManager.instance.GetResourceManagement().GetResourceNumber(GameEnum.ResourceType.Arrow) - GameManager.instance.GetResourceManagement().GetArrowMinusNumber() < 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            public static bool HaveEnoughHourse()
            {
                if (GameManager.instance.GetResourceManagement().GetResourceNumber(GameEnum.ResourceType.Hourse) - GameManager.instance.GetResourceManagement().GetHourseMinusNumber() < 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static class Camera
        {
            public static void CameraSwiftMove()
            {
                GameManager.instance.GetRTSCamera().keyboardMovementSpeed = GameManager.instance.GetCameraSwiftMoveSpeed();
            }

            public static void CameraNormalMove()
            {
                GameManager.instance.GetRTSCamera().keyboardMovementSpeed = GameManager.instance.GetCameraNormalMoveSpeed();
            }

        }

        #region  Get References
        public static ResourceManagement GetResourceManagement()
        {
            return GameManager.instance.GetResourceManagement();
        }

        public static SpawnManagement GetSpawnManagement()
        {
            return GameManager.instance.GetSpawnManagement();
        }
        #endregion


        #region Player Status
        public static void SetPlayerInTheHourseMine(bool _inTheMine)
        {
            GameManager.instance.GetResourceManagement().SetInTheHourseMine(_inTheMine);
        }

        public static void SetPlayerInTheGoldMine(bool _inTheMine)
        {
            GameManager.instance.GetResourceManagement().SetInTheGoldMine(_inTheMine);
        }

        public static void SetPlayerInTheArrowMine(bool _inTheMine)
        {
            GameManager.instance.GetResourceManagement().SetInTheArrowMine(_inTheMine);
        }

        public static bool SetResourceMinus(GameEnum.CharacterType _characterType)
        {
            switch (_characterType)
            {
                case GameEnum.CharacterType.Saber:
                    return GameManager.instance.GetResourceManagement().SetSaberMinus();
                case GameEnum.CharacterType.Archor:
                    return GameManager.instance.GetResourceManagement().SetArchorMinus();
                case GameEnum.CharacterType.Rider:
                    return GameManager.instance.GetResourceManagement().SetRiderMinus();
                default:
                    Debug.LogError("CharacterType error");
                    return false;
            }
        }
        #endregion
        
    }

}
