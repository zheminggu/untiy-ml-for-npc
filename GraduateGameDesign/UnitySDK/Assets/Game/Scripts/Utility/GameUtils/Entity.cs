﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Adamgu.GameUtils
{
    public class Entity
    {
        #region  Entitys
        public static Unity.Entities.Entity GetEntityOfGameObject(GameObject gameObject)
        {
            return gameObject.GetComponent<GameObjectEntity>().Entity;
        }

        public static CharacterStatusComponent GetCharacterStatusComponent(GameObject gameObject)
        {
            return gameObject.GetComponent<CharacterStatusComponent>();
        }

        public static CharacterAnimationComponent GetCharacterAnimationComponent(GameObject gameObject)
        {
            return gameObject.GetComponent<CharacterAnimationComponent>();
        }

        public static CharacterAnimationEventSystem GetCharacterAnimationEventSystem(GameObject gameObject)
        {
            return gameObject.GetComponent<CharacterAnimationEventSystem>();
        }

        public static CharacterImpactComponent GetCharacterImpactComponent(GameObject gameObject)
        {
            return GameManager.instance.GetEntityManager().GetComponentData<CharacterImpactComponent>(gameObject.GetComponent<GameObjectEntity>().Entity);
        }

        public static EntityManager GetEntityManager()
        {
            return GameManager.instance.GetEntityManager();
        }

        public static CharacterMoveComponent GetCharacterMoveComponent(GameObject gameObject)
        {
            return gameObject.GetComponent<CharacterMoveComponent>();
        }

        public static ChraracterDamageComponent GetCharacterDamageComponent(GameObject gameObject)
        {
            return gameObject.GetComponent<ChraracterDamageComponent>();
        }

        public static HouseComponent GetHouseComponent(GameObject gameObject)
        {
            return gameObject.GetComponent<HouseComponent>();
        }

        public static bool GameObjectEntityHasComponent<T>(GameObject gameObject)
        {
            //var entity = GetEntityOfGameObject(gameObject);
            //var entityManager = GetEntityManager();
            return GetEntityManager().HasComponent<T>(GetEntityOfGameObject(gameObject));
        }
        #endregion
    }
}

