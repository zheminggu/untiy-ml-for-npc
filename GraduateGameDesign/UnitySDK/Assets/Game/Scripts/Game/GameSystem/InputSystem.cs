﻿
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace Adamgu
{
    public class InputSystem : ComponentSystem
    {
        private struct CharacterMoveData
        {
            public readonly int Length;
            public ComponentArray<Transform> Transform;
            public ComponentArray<CharacterMoveComponent> CharacterMoveComponent;
            public ComponentArray<CharacterAnimationComponent> CharacterAnimationComponent;
            public ComponentArray<CharacterStatusComponent> CharacterStatusComponent;
        }



        [Inject] CharacterMoveData _characterMoveData;


        protected override void OnUpdate()
        {
            var hitButtonDown = Input.GetKeyDown(KeyCode.Q);
            var mouseRightButtonDown = Input.GetMouseButtonDown(1);
            var mousePosition = Input.mousePosition;
            var cameraRay = Camera.main.ScreenPointToRay(mousePosition);
            var layerMask = LayerMask.GetMask("Ground");
            var random = new Unity.Mathematics.Random((uint)UnityEngine.Random.Range(1, 100000)).NextInt(1, 9);
            if (Physics.Raycast(cameraRay, out var hit, 100, layerMask))
            {
                for (int i = 0; i < _characterMoveData.Length; i++)
                {
                    switch (GameUtils.GameManagement.Character.GetCharacterType(_characterMoveData.CharacterStatusComponent[i].CharacterType))
                    {
                        case GameEnum.CharacterType.Saber:
                            if (mouseRightButtonDown)
                            {

                                if (_characterMoveData.CharacterStatusComponent[i].Selected)
                                {
                                    //Debug.Log("command move to target");
                                    _characterMoveData.CharacterMoveComponent[i].TargetPosition = hit.point;
                                    _characterMoveData.CharacterMoveComponent[i].NeedMove = true;
                                }

                            }
                            if (hitButtonDown)
                            {
                                if (_characterMoveData.CharacterStatusComponent[i].Selected)
                                {
                                    _characterMoveData.CharacterAnimationComponent[i].HitTrigger = random;
                                    _characterMoveData.CharacterMoveComponent[i].NeedMove = false;
                                    _characterMoveData.CharacterMoveComponent[i].TargetPosition = _characterMoveData.Transform[i].position;
                                }
                            } 
                            break;
                        case GameEnum.CharacterType.Archor:
                            if (mouseRightButtonDown)
                            {

                                if (_characterMoveData.CharacterStatusComponent[i].Selected)
                                {
                                    //Debug.Log("command move to target");
                                    _characterMoveData.CharacterMoveComponent[i].TargetPosition = hit.point;
                                    _characterMoveData.CharacterMoveComponent[i].NeedMove = true;
                                }

                            }
                            if (hitButtonDown)
                            {
                                if (_characterMoveData.CharacterStatusComponent[i].Selected)
                                {
                                    _characterMoveData.CharacterAnimationComponent[i].HitTrigger = random;
                                    _characterMoveData.CharacterMoveComponent[i].NeedMove = false;
                                    _characterMoveData.CharacterMoveComponent[i].TargetPosition = _characterMoveData.Transform[i].position;
                                }
                            }
                            break;
                        case GameEnum.CharacterType.Rider:
                            if (mouseRightButtonDown)
                            {

                                if (_characterMoveData.CharacterStatusComponent[i].Selected)
                                {
                                    //Debug.Log("command move to target");
                                    _characterMoveData.CharacterMoveComponent[i].TargetPosition = hit.point;
                                    _characterMoveData.CharacterMoveComponent[i].NeedMove = true;
                                }

                            }
                            if (hitButtonDown)
                            {
                                if (_characterMoveData.CharacterStatusComponent[i].Selected)
                                {
                                    _characterMoveData.CharacterAnimationComponent[i].HitTrigger = random;
                                    _characterMoveData.CharacterMoveComponent[i].NeedMove = false;
                                    _characterMoveData.CharacterMoveComponent[i].TargetPosition = _characterMoveData.Transform[i].position;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                  
                }

            }

        }




    }
}

