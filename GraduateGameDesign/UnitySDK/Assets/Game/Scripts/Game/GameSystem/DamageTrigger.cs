﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Adamgu
{
    public class DamageTrigger : MonoBehaviour
    {
        public GameObject Owner;
        #region Event Function
        private void OnTriggerEnter(Collider other)
        {
            //Debug.Log(other);
            if (other.tag=="Character")
            {
                AddOrUpdateImpact(other, GameUtils.Entity.GetCharacterDamageComponent(gameObject).Value);
            }
            
        }

        //private void OnTriggerExit(Collider other)
        //{

        //}

        //private void OnTriggerStay(Collider other)
        //{

        //}


        private void AddOrUpdateImpact(Collider other, int Damage)
        {
            var collidingEntity = GameUtils.Entity.GetEntityOfGameObject(other.gameObject);
            //character in the same side
            if (GameUtils.Entity.GetCharacterStatusComponent(other.gameObject).CharacterSide == GameUtils.Entity.GetCharacterStatusComponent(Owner).CharacterSide)
            {
                return;
            }
            if (!GameUtils.Entity.GetEntityManager().HasComponent<CharacterImpactComponent>(collidingEntity))
            {
                GameUtils.Entity.GetEntityManager().AddComponent(collidingEntity, typeof(CharacterImpactComponent));
            }
            GameUtils.Entity.GetEntityManager().SetComponentData(collidingEntity, new CharacterImpactComponent
            {
                Value = Damage
            });
        }
        #endregion
    }

}
