﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Adamgu
{
    public class ImpactClearStstem : ComponentSystem
    {
        private struct Data
        {
            public readonly int Length;
            public ComponentDataArray<CharacterImpactComponent> CharacterImpactComponent;
            public EntityArray EntityArray;
        }
        [Inject] Data _data;
        protected override void OnUpdate()
        {
            for (int i = 0; i < _data.Length; i++)
            {
                PostUpdateCommands.RemoveComponent<CharacterImpactComponent>(_data.EntityArray[i]);
            }
        }
    }
}

