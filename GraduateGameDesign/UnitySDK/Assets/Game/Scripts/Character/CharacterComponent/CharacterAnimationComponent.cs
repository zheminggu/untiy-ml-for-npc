﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    public class CharacterAnimationComponent : MonoBehaviour
    {
        public float VelocityX;
        public float VelocityY;

        public int HitTrigger;
        public bool FinishedHitting;
        public float HitTimer;


    }
}

