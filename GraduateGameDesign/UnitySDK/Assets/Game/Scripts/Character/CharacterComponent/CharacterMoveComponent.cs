﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace Adamgu
{
    public class CharacterMoveComponent : MonoBehaviour
    {
        public float3 TargetPosition;
        public bool NeedMove;
        public bool Selected;
        public float StopRange=0.5f;

    }
}

