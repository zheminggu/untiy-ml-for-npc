﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Adamgu
{
    public struct CharacterImpactComponent : IComponentData
    {
        public int Value;
    }

}

