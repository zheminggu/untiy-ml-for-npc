﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Adamgu
{
    public class CharacterStatusComponent : MonoBehaviour
    {
        //0 saber 1 archor 2 rider
        [InfoBox("0 saber 1 archor 2 rider")]
        [PropertyRange(0, 2)]
        public int CharacterType;
        [InfoBox("0 Blue side 1 RedSide")]
        [PropertyRange(0,1)]
        public int CharacterSide;

        [HideInInspector]
        public bool Selected;
        [HideInInspector]
        public bool SelfControl;

        public int Health;
        public bool Dead;

    }
}

