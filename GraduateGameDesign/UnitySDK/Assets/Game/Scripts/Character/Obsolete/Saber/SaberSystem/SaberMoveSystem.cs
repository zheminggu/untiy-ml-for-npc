﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.AI;


namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class SaberMoveSystem : ComponentSystem
    {
        private struct Filter
        {
            public SaberMoveComponent CharacterMoveComponent;
            public SaberAnimationComponent CharacterAnimationComponent;
            public NavMeshAgent NavMeshAgent;
        }
        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {
                if (item.CharacterMoveComponent.NeedMove)
                {
                    //Debug.Log("Need to move");
                    //move to destination
                    item.NavMeshAgent.isStopped = false;
                    item.NavMeshAgent.SetDestination(item.CharacterMoveComponent.TargetPosition);
                }
                else
                {
                    item.NavMeshAgent.isStopped = true;
                }
            }
        }

       
    }
}

