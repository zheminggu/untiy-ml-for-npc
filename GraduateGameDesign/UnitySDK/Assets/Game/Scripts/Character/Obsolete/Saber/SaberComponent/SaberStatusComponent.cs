﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class SaberStatusComponent : MonoBehaviour
    {
        public bool Selected;
        /// <summary>
        /// True is Blue side False is Red Side
        /// </summary>
        public bool Side;

        public int Health;
    }
}


