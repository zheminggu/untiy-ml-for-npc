﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Unity.Entities;
using Unity.Mathematics;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class SaberArriveSystem : ComponentSystem
    {
        private struct Filter
        {
            public NavMeshAgent NavMeshAgent;
            public SaberMoveComponent SaberMoveComponent;
            public SaberStatusComponent SaberStatusComponent;
            public Transform Transform;
        }

        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {
                if (item.SaberMoveComponent.NeedMove)
                {
                    //Debug.Log(GameUtils.SqrMagnitude((GameUtils.Vec3ToFloat3(item.Transform.position) - item.SaberMoveComponent.TargetPosition)));
                    //one saber reached target point
                    if (GameUtils.Math.SqrMagnitude((GameUtils.Math.Vec3ToFloat3(item.Transform.position)-item.SaberMoveComponent.TargetPosition)) < 0.5f)
                    {
                        //Debug.Log("Arrived Target");

                        foreach (var entity in GetEntities<Filter>())
                        {
                            if (item.SaberMoveComponent.NeedMove)
                            {
                                entity.SaberMoveComponent.NeedMove = false;
                            }
                        }
                        return;
                    }
                }
            }
        }

       
    }

}
