﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class SaberStatusSystem : ComponentSystem
    {
        private struct StatusData
        {
            public readonly int Length;
            public ComponentArray<SaberStatusComponent> CharacterStatusComponent;
            public ComponentArray<Transform> Tramsform;
        }
        [Inject] StatusData _statusData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < _statusData.Length; i++)
            {
                if (GameInputManager.instance.IsSelecting())
                {
                    _statusData.CharacterStatusComponent[i].Selected = IsWithinSelectionBounds(_statusData.Tramsform[i]);
                }
            }

        }


        public bool IsWithinSelectionBounds(Transform _transform)
        {

            var camera = Camera.main;
            var viewportBounds = UIUtils.GetViewportBounds(camera, GameInputManager.instance.GetMouseStartPosition(), Input.mousePosition);
            return viewportBounds.Contains(camera.WorldToViewportPoint(_transform.position));
        }
    }
}
