﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;


namespace Adamgu {
    [System.Obsolete("Please use Character Component instead")]
    public class SaberMoveComponent : MonoBehaviour
    {
        public float3 TargetPosition;
        public bool NeedMove;
        public bool Selected;
    }


}


