﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class SaberAnimationEventSystem : MonoBehaviour
    {
        public GameObject SelectRing;
        private SaberStatusComponent characterStatusComponent;

        private void Start()
        {
            SelectRing.SetActive(false);
            characterStatusComponent = GetComponent<SaberStatusComponent>();
        }

        private void Update()
        {
            if (characterStatusComponent.Selected)
            {
                SelectRing.SetActive(true);
            }
            else
            {
                SelectRing.SetActive(false);
            }
        }
    }

}
