﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Unity.Entities;


namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class SaberAnimationSystem : ComponentSystem
    {
        private struct Filter
        {
            public SaberAnimationComponent CharacterAnimationComponent;
            public Animator Animator;
            public NavMeshAgent NavMeshAgent;
        }
        protected override void OnUpdate()
        {

            foreach (var entity in GetEntities<Filter>())
            {
                if (entity.NavMeshAgent.velocity.sqrMagnitude != 0)
                {
                    entity.Animator.SetBool("Moving", true);
                    entity.Animator.SetFloat("Velocity X", entity.NavMeshAgent.velocity.x);
                    entity.Animator.SetFloat("Velocity Z", entity.NavMeshAgent.velocity.z);
                }
                else
                {
                    entity.Animator.SetBool("Moving", false);
                    entity.Animator.SetFloat("Velocity X", 0);
                    entity.Animator.SetFloat("Velocity Z", 0);
                }

                switch (entity.CharacterAnimationComponent.HitTrigger)
                {
                    case 0:

                        break;
                    case 1:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger1");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 2:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger2");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 3:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger3");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 4:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger4");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 5:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger5");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 6:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger6");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 7:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger7");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 8:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger8");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    case 9:
                        entity.CharacterAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger9");
                        entity.CharacterAnimationComponent.HitTrigger = 0;
                        break;
                    default:
                        break;
                }

                if (!entity.CharacterAnimationComponent.FinishedHitting)
                {
                    entity.CharacterAnimationComponent.HitTimer += Time.deltaTime;
                    if (entity.CharacterAnimationComponent.HitTimer > 1)
                    {
                        entity.CharacterAnimationComponent.HitTimer = 0;
                        entity.CharacterAnimationComponent.FinishedHitting = true;
                    }
                }

            }

        }
    }
}

