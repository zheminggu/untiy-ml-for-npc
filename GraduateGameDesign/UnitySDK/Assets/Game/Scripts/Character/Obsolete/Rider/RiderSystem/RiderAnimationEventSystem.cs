﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    [System.Obsolete("Please use Character System instead")]
    public class RiderAnimationEventSystem : MonoBehaviour
    {
        public GameObject SelectRing;
        public Animator CharacterAnimator;

        private RiderStatusComponent riderStatusComponent;
        private RiderAnimationComponent riderAnimationComponent;
        
        private void Start()
        {
            SelectRing.SetActive(false);
            riderStatusComponent = GetComponent<RiderStatusComponent>();
            riderAnimationComponent = GetComponent<RiderAnimationComponent>();
        }

        private void Update()
        {
            if (riderStatusComponent.Selected)
            {
                SelectRing.SetActive(true);
            }
            else
            {
                SelectRing.SetActive(false);
            }

            switch (riderAnimationComponent.HitTrigger)
            {
                case 0:

                    break;
                case 1:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger1");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 2:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger2");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 3:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger3");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 4:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger4");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 5:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger5");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 6:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger6");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 7:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger7");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 8:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger8");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                case 9:
                    riderAnimationComponent.FinishedHitting = false;
                    CharacterAnimator.SetTrigger("AttackTrigger9");
                    riderAnimationComponent.HitTrigger = 0;
                    break;
                default:
                    break;
            }

            if (!riderAnimationComponent.FinishedHitting)
            {
                riderAnimationComponent.HitTimer += Time.deltaTime;
                if (riderAnimationComponent.HitTimer > 1)
                {
                    riderAnimationComponent.HitTimer = 0;
                    riderAnimationComponent.FinishedHitting = true;
                }
            }
        }
    }
}

