﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class RiderStatusComponent : MonoBehaviour
    {
        public bool Selected;

        public int Health;
    }
}

