﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class RiderAnimationSystem : ComponentSystem
    {
        private struct Filter
        {
            public RiderAnimationComponent RiderAnimationComponent;
            public Animator Animator;
            public NavMeshAgent NavMeshAgent;
        }
        protected override void OnUpdate()
        {

            foreach (var entity in GetEntities<Filter>())
            {
                if (entity.NavMeshAgent.velocity.sqrMagnitude != 0)
                {
                    entity.Animator.SetBool("isRunning", true);
                }
                else
                {
                    entity.Animator.SetBool("isRunning", false);

                    //entity.Animator.SetFloat("Vertical", 0);
                    //entity.Animator.SetFloat("Horizontal", 0);
                }

                //switch (entity.RiderAnimationComponent.HitTrigger)
                //{
                //    case 0:

                //        break;
                //    case 1:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger1");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 2:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger2");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 3:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger3");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 4:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger4");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 5:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger5");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 6:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger6");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 7:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger7");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 8:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger8");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    case 9:
                //        entity.RiderAnimationComponent.FinishedHitting = false;
                //        entity.Animator.SetTrigger("AttackTrigger9");
                //        entity.RiderAnimationComponent.HitTrigger = 0;
                //        break;
                //    default:
                //        break;
                //}

                //if (!entity.RiderAnimationComponent.FinishedHitting)
                //{
                //    entity.RiderAnimationComponent.HitTimer += Time.deltaTime;
                //    if (entity.RiderAnimationComponent.HitTimer > 1)
                //    {
                //        entity.RiderAnimationComponent.HitTimer = 0;
                //        entity.RiderAnimationComponent.FinishedHitting = true;
                //    }
                //}

            }

        }
    }

}

