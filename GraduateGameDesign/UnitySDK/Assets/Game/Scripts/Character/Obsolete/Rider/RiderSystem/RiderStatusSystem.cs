﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;


namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class RiderStatusSystem : ComponentSystem
    {
        private struct StatusData
        {
            public readonly int Length;
            public ComponentArray<RiderStatusComponent> RiderStatusComponent;
            public ComponentArray<Transform> Tramsform;
        }
        [Inject] StatusData _statusData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < _statusData.Length; i++)
            {
                if (GameInputManager.instance.IsSelecting())
                {
                    _statusData.RiderStatusComponent[i].Selected = IsWithinSelectionBounds(_statusData.Tramsform[i]);
                }
            }

        }


        private bool IsWithinSelectionBounds(Transform _transform)
        {

            var camera = Camera.main;
            var viewportBounds = UIUtils.GetViewportBounds(camera, GameInputManager.instance.GetMouseStartPosition(), Input.mousePosition);
            return viewportBounds.Contains(camera.WorldToViewportPoint(_transform.position));
        }
    }
}

