﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class RiderMoveSystem :ComponentSystem
    {
        private struct Filter
        {
            public RiderMoveComponent RiderMoveComponent;
            public RiderAnimationComponent RiderAnimationComponent;
            public NavMeshAgent NavMeshAgent;
        }

        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {

                if (item.RiderMoveComponent.NeedMove)
                {
                    item.NavMeshAgent.SetDestination(item.RiderMoveComponent.TargetPosition);
                    item.RiderMoveComponent.NeedMove = false;
                }
                else
                {
                    item.NavMeshAgent.isStopped = false;
                }
            }
        }
    }
}

