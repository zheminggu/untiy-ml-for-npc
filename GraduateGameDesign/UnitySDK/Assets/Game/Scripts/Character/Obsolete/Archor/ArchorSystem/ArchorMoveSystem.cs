﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine.AI;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class ArchorMoveSystem : ComponentSystem
    {
        private struct Filter
        {
            public ArchorMoveComponent ArchorMoveComponent;
            public ArchorAnimationComponent ArchorAnimationComponent;
            public NavMeshAgent NavMeshAgent;
        }
        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {

                item.NavMeshAgent.isStopped = !item.ArchorAnimationComponent.FinishedHitting;

                if (item.ArchorMoveComponent.NeedMove)
                {
                    item.NavMeshAgent.SetDestination(item.ArchorMoveComponent.TargetPosition);
                    item.ArchorMoveComponent.NeedMove = false;
                }
            }
        }
    }

}
