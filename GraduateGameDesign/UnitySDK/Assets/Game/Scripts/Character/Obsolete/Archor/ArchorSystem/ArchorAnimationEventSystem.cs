﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    [System.Obsolete("Please use Character System instead")]
    public class ArchorAnimationEventSystem : MonoBehaviour
    {

        public GameObject SelectRing;
        private ArchorStatusComponent archorStatusComponent;

        private void Start()
        {
            SelectRing.SetActive(false);
            archorStatusComponent = GetComponent<ArchorStatusComponent>();
        }

        private void Update()
        {
            if (archorStatusComponent.Selected)
            {
                SelectRing.SetActive(true);
            }
            else
            {
                SelectRing.SetActive(false);
            }
        }
    }
}

