﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.AI;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class ArchorAnimationSystem : ComponentSystem
    {
        private struct Filter
        {
            public ArchorAnimationComponent ArchorAnimationComponent;
            public Animator Animator;
            public NavMeshAgent NavMeshAgent;
        }
        protected override void OnUpdate()
        {

            foreach (var entity in GetEntities<Filter>())
            {
                if (entity.NavMeshAgent.velocity.sqrMagnitude != 0)
                {
                    entity.Animator.SetBool("Moving", true);
                    entity.Animator.SetFloat("Velocity X", entity.NavMeshAgent.velocity.x);
                    entity.Animator.SetFloat("Velocity Z", entity.NavMeshAgent.velocity.z);
                }
                else
                {
                    entity.Animator.SetBool("Moving", false);
                    entity.Animator.SetFloat("Velocity X", 0);
                    entity.Animator.SetFloat("Velocity Z", 0);
                }

                switch (entity.ArchorAnimationComponent.HitTrigger)
                {
                    case 0:

                        break;
                    case 1:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger1");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 2:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger2");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 3:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger3");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 4:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger4");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 5:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger5");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 6:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger6");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 7:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger7");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 8:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger8");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    case 9:
                        entity.ArchorAnimationComponent.FinishedHitting = false;
                        entity.Animator.SetTrigger("AttackTrigger9");
                        entity.ArchorAnimationComponent.HitTrigger = 0;
                        break;
                    default:
                        break;
                }

                if (!entity.ArchorAnimationComponent.FinishedHitting)
                {
                    entity.ArchorAnimationComponent.HitTimer += Time.deltaTime;
                    if (entity.ArchorAnimationComponent.HitTimer > 1)
                    {
                        entity.ArchorAnimationComponent.HitTimer = 0;
                        entity.ArchorAnimationComponent.FinishedHitting = true;
                    }
                }

            }

        }


    }

}
