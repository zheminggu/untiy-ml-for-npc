﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class ArchorArriveSystem : ComponentSystem
    {
        private struct Filter
        {
            public NavMeshAgent NavMeshAgent;
            public ArchorMoveComponent ArchorMoveComponent;
            public ArchorStatusComponent ArchorStatusComponent;
            public Transform Transform;
        }

        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {
                if (item.ArchorMoveComponent.NeedMove)
                {
                   
                    if (GameUtils.Math.SqrMagnitude((GameUtils.Math.Vec3ToFloat3(item.Transform.position) - item.ArchorMoveComponent.TargetPosition)) < 0.5f)
                    {
                     
                        foreach (var entity in GetEntities<Filter>())
                        {
                            if (item.ArchorMoveComponent.NeedMove)
                            {
                                entity.ArchorMoveComponent.NeedMove = false;
                            }
                        }
                        return;
                    }
                }
            }
        }

    }

}
