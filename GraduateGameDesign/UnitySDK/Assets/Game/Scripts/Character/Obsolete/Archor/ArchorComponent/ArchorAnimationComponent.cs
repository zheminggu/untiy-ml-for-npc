﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class ArchorAnimationComponent : MonoBehaviour
    {
        public float VelocityX;
        public float VelocityY;

        public int HitTrigger;
        public bool FinishedHitting;
        public float HitTimer;
    }
}


