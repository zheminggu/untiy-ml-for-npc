﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Adamgu
{
    [System.Obsolete("Please use Character Component instead")]
    public class ArchorMoveComponent : MonoBehaviour
    {
        public float3 TargetPosition;
        public bool NeedMove;
        public bool Selected;

    }

}
