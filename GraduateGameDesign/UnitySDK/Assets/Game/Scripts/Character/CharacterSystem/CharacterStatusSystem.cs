﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Adamgu {
    public class CharacterStatusSystem : ComponentSystem
    {
        private struct StatusData
        {
            public readonly int Length;
            public ComponentArray<CharacterStatusComponent> CharacterStatusComponent;
            public ComponentArray<Transform> Tramsform;
        }
        [Inject] StatusData _statusData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < _statusData.Length; i++)
            {
                if (GameInputManager.instance.IsSelecting())
                {
                    //character can be chosen when it;s belongs to players side, at the same time it's in the selection bounds
                    if (IsWithinSelectionBounds(_statusData.Tramsform[i])&&GameUtils.GameManagement.Character.CharacterSideEquals(_statusData.CharacterStatusComponent[i].CharacterSide,GameManager.instance.GetGameSide()))
                    {
                        _statusData.CharacterStatusComponent[i].Selected = true;
                    }
                    else
                    {
                        _statusData.CharacterStatusComponent[i].Selected = false;
                    }
                }
            }

        }


        public bool IsWithinSelectionBounds(Transform _transform)
        {
            var camera = Camera.main;
            var viewportBounds = UIUtils.GetViewportBounds(camera, GameInputManager.instance.GetMouseStartPosition(), Input.mousePosition);
            return viewportBounds.Contains(camera.WorldToViewportPoint(_transform.position));
        }
    }
}



