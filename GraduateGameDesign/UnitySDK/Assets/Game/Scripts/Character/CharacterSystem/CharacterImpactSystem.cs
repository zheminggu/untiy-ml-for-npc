﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Adamgu
{
    public class CharacterImpactSystem :ComponentSystem
    {
        private struct ImpactData
        {
            public readonly int Length;
            public ComponentDataArray<CharacterImpactComponent>  CharacterImpactComponent;
            public ComponentArray<CharacterStatusComponent> CharacterStatusComponent;
            public EntityArray Entity;
        }

        [Inject] ImpactData _impactData;

        protected override void OnUpdate()
        {
            for (int i = 0; i < _impactData.Length; i++)
            {
                if (!_impactData.CharacterStatusComponent[i])
                {
                    continue;
                }
                _impactData.CharacterStatusComponent[i].Health -= _impactData.CharacterImpactComponent[i].Value;
            }
        }

      
    }

}
