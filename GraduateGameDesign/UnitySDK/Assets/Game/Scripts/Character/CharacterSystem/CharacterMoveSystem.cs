﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.AI;

namespace Adamgu
{
    public class CharacterMoveSystem : ComponentSystem
    {
        private struct Filter
        {
            public CharacterMoveComponent CharacterMoveComponent;
            public CharacterAnimationComponent CharacterAnimationComponent;
            public NavMeshAgent NavMeshAgent;
        }
        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {
                if (item.CharacterMoveComponent.NeedMove)
                {
                    item.NavMeshAgent.isStopped = false;
                    item.NavMeshAgent.SetDestination(item.CharacterMoveComponent.TargetPosition);
                }
                else
                {
                    item.NavMeshAgent.isStopped = true;
                }
            }
        }


    }

}
