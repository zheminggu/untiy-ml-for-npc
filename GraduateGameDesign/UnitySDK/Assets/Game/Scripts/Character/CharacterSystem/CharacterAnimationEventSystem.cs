﻿using Unity.Entities;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Adamgu
{

    public class CharacterAnimationEventSystem : MonoBehaviour
    {

        public GameObject SelectRing;

        [InfoBox("Only need when character is rider")]
        public Animator CharacterAnimator;
        private CharacterStatusComponent characterStatusComponent;
        private CharacterAnimationComponent characterAnimationComponent;

        private void Start()
        {
            SelectRing.SetActive(false);
            characterStatusComponent = GameUtils.Entity.GetCharacterStatusComponent(gameObject);
            if (GameUtils.GameManagement.Character.CharacterTypeEquals(characterStatusComponent.CharacterType,GameEnum.CharacterType.Rider))
            {
                characterAnimationComponent =GameUtils.Entity.GetCharacterAnimationComponent(gameObject);
                if (!CharacterAnimator)
                {
                    CharacterAnimator = GetComponentInChildren<Animator>();
                }
            }
          
        }

        private void Update()
        {
            if (characterStatusComponent.Selected)
            {
                SelectRing.SetActive(true);
            }
            else
            {
                SelectRing.SetActive(false);
            }
            
            if (GameUtils.GameManagement.Character.CharacterTypeEquals(characterStatusComponent.CharacterType,GameEnum.CharacterType.Rider))
            {
                switch (characterAnimationComponent.HitTrigger)
                {
                    case 0:

                        break;
                    case 1:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger1");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 2:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger2");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 3:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger3");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 4:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger4");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 5:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger5");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 6:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger6");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 7:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger7");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 8:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger8");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    case 9:
                        characterAnimationComponent.FinishedHitting = false;
                        CharacterAnimator.SetTrigger("AttackTrigger9");
                        characterAnimationComponent.HitTrigger = 0;
                        break;
                    default:
                        break;
                }

                if (!characterAnimationComponent.FinishedHitting)
                {
                    characterAnimationComponent.HitTimer += Time.deltaTime;
                    if (characterAnimationComponent.HitTimer > 1)
                    {
                        characterAnimationComponent.HitTimer = 0;
                        characterAnimationComponent.FinishedHitting = true;
                    }
                }
            }
        }


        #region Animation Event Function
        public void Hit()
        {

        }
        public void FootR()
        {
        }
        public void FootL()
        {
        }
        public void Land()
        {


        }
        public void Shooting()
        {

        }
        public void WeaponSwitch()
        {
        }
        public void FinishedHitting()
        {

        }

        #endregion

      
    }
}

