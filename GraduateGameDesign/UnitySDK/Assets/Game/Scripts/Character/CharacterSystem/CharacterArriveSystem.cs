﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.AI;

namespace Adamgu
{
    public class CharacterArriveSystem : ComponentSystem
    {
        private struct Filter
        {
            public NavMeshAgent NavMeshAgent;
            public CharacterMoveComponent CharacterMoveComponent;
            public CharacterStatusComponent CharacterStatusComponent;
            public Transform Transform;
        }

        protected override void OnUpdate()
        {
            foreach (var item in GetEntities<Filter>())
            {
                //only control character on the same side of the player
                if (GameUtils.GameManagement.Character.CharacterSideEquals(item.CharacterStatusComponent.CharacterSide,GameManager.instance.GetGameSide()))
                {
                    if (item.CharacterMoveComponent.NeedMove)
                    {
                        //Debug.Log(GameUtils.SqrMagnitude((GameUtils.Vec3ToFloat3(item.Transform.position) - item.SaberMoveComponent.TargetPosition)));
                        //one character which is not rider reached target point
                        if (ArrivedTarget(item) && !CharacterIsRider(item))
                        {
                            //Debug.Log("Arrived Target");

                            foreach (var entity in GetEntities<Filter>())
                            {
                                if (entity.CharacterMoveComponent.NeedMove && !CharacterIsRider(entity))
                                {
                                    entity.CharacterMoveComponent.NeedMove = false;
                                    entity.CharacterMoveComponent.StopRange = 0.5f;
                                }
                            }
                            return;
                        }
                        else if (ArrivedTarget(item) && CharacterIsRider(item))
                        {
                            foreach (var entity in GetEntities<Filter>())
                            {
                                if (entity.CharacterMoveComponent.NeedMove && CharacterIsRider(entity))
                                {
                                    entity.CharacterMoveComponent.NeedMove = false;
                                }
                                else if (entity.CharacterMoveComponent.NeedMove && !CharacterIsRider(entity))
                                {
                                    entity.CharacterMoveComponent.StopRange = 2f;

                                }
                            }
                            return;
                        }

                    }
                }
              
            }
        }

        private bool ArrivedTarget(Filter item)
        {
            return GameUtils.Math.SqrMagnitude((GameUtils.Math.Vec3ToFloat3(item.Transform.position) - item.CharacterMoveComponent.TargetPosition)) < item.CharacterMoveComponent.StopRange* item.CharacterMoveComponent.StopRange;
        }

        private bool CharacterIsRider(Filter item)
        {
            return GameUtils.GameManagement.Character.CharacterTypeEquals(item.CharacterStatusComponent.CharacterType, GameEnum.CharacterType.Rider);
        }
      
    }
}

