﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace Adamgu
{
    public class CharacterDeathSystem : ComponentSystem
    {
        private struct CharacterData
        {
            public readonly int Length;
            public ComponentDataArray<CharacterImpactComponent> CharacterImpactComponent;
            public ComponentArray<CharacterStatusComponent> CharacterStatusComponent;
            public ComponentArray<Animator> CharacterAnimatorComponent;
        }

        [Inject] CharacterData _characterData;
        protected override void OnUpdate()
        {
            for (int i = 0; i < _characterData.Length; i++)
            {
                if (!_characterData.CharacterStatusComponent[i])
                {
                    continue;
                }
                if (_characterData.CharacterStatusComponent[i].Health <= 0&&!_characterData.CharacterStatusComponent[i].Dead)
                {
                    //now this character is dead
                    _characterData.CharacterStatusComponent[i].Dead = true;

                    //play dead animation
                    _characterData.CharacterAnimatorComponent[i].SetTrigger("DeathTrigger1");

                    //waiting for some time then destory
                    Object.Destroy(_characterData.CharacterStatusComponent[i].gameObject,4f);
                }
            }
        }

       
    }

}
